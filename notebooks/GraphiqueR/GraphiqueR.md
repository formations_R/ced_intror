
Les graphiques constituent souvent le point de départ d'une analyse statistique, R fournit des moyens simples pour produire des graphiques de base de toute nature. On verra dans ce cours, l'étude des graphiques conventionnels. Des graphiques plus complexes sont possibles avec les packages **lattices** et **ggplot2**, on abordera brièvement les fonctionnalités sans rentrer dans les détails techniques. Des références de tutoriaux vous seront fournies.

Pour commencer il peut être intéressant de regarder quelques exemples de représentaions graphiques pouvant être construites avec R, vous utiliserez la fonction **demo**:


```R
demo(graphics)  
```

## Les fonctions graphiques de base

Les fonctions couramment utilisées.

* Variables qualitatives <br />
**pie(x)** # diagramme camenbert <br />
**barplot(x)** # diagramme bâton 


* Variables quantitatives <br />
**hist(x,nclass)** # histogramme de x <br />
**boxplot(x)** # boite à moustache <br />
**stripchart**(x) <br />


* Graphiques 3D <br />
**image(x,y,z)** # forme d'image <br />
**persp(x,y,z)** # forme de nappe c <br />
**contour(x,y,z)** # les contours <br />
**Fonction utile** : z=outer(x,y,fonction)


* Tableau et matrices<br />
**pairs(data)** # nuage de points colonne par colonne de data<br />
**matplot(data)** # trace chaque colonne de data

### La fonction plot

La fonction **plot** est une fonction générique de R permettant de représenter tous les types de données.

#### Nuage de points d'une variable y en fonction d'une variable x

Représenter à intervalles réguliers les points de la fonction: $\bf{ x \longrightarrow sin(2\pi x) \quad x \in [0,1[}$


```R
# Construire la grille de points
grillex <- seq(0,1,length=50)
fx <- sin(2*pi*grillex)
plot(x=grillex,y=fx)
```

Il est plus classique d'utiliser la fonction plot avec des formules du type  $\bf{y^{\sim}x}$


```R
plot(fx~grillex)
```

#### A partir d'yn data-frame

On reprend le fichier de données **mtcars**

   Description : les données ont été extraites du magazine Motor Trend US de 1974 et comprennent la consommation de carburant et 10 aspects de la conception et des performances de 32 automobiles (modèles 1973-1974).

   Format:

Un fichier de données "mtcars" avec 32 observations sur 11 variables:

mpg: Miles/(US) gallon (variable quantitative)
cyl: Number of cylinders (variable qualitative)
disp: Displacement (cu.in.) (variable quantitative)
hp:    Gross horsepower (variable quantitative)
drat: Rear axle ratio (variable quantitative)
wt:    Weight (1000 lbs) (variable quantitative)
qsec: 1/4 mile time (variable quantitative)
vs: V/S (variable qualitative)
am:    Transmission (0 = automatic, 1 = manual) (variable qualitative)
gear: Number of forward gears (variable qualitative)
carb: Number of carburetors (variable qualitative)



```R
mtcars <- read.csv("data/mtcars.csv",header=TRUE,sep=",")
str(mtcars)
summary(mtcars)
# Definir en tant que variable qualitative
mtcars$cyl <- factor(mtcars$cyl)
mtcars$vs <- factor(mtcars$vs)
mtcars$am <- factor(mtcars$am)
mtcars$gear <- factor(mtcars$gear)
mtcars$carb <- factor(mtcars$carb)
summary(mtcars)
```

* Représentation de deux variables qualitatives: les variables **drat** et **wt**


```R
plot(mtcars[,"drat"],mtcars[,"wt"])
```


![png](output_9_0.png)


Comme les deux variables sont contenues dans le même *data-frame*, une syntaxe plus simple permet d'avoir directement les noms de variables en labellés d'axe.


```R
plot(drat~wt,data=mtcars)
```

Cette représenation graphique aurait pu être obtenue en définissant de manière explicite les labels des axes à l'appel de la fonction "plot" avec les arguments **xlab** et **ylab**.


```R
plot(mtcars[,"drat"],mtcars[,"wt"],xlab="wt",ylab="drat")
```

* Pour représenter une variable quantitative ("wt") en fonction d'une variable qualitative ("cyl"), on utilisera la même syntaxe:


```R
plot(wt~cyl,data=mtcars,xlab="Number of cylinders",ylab="Weight (1000 lbs)")
```


![png](output_15_0.png)


La fonction plot retourne une boîte à moustaches par modalité de la variable qualitative. Ce graphique permet de voir rapidement si il existe un effet de la variable *Numbers of cylinders* sur la varibale *Weighth*. Il peut être obtenu à partir de la variable **boxplot**.


```R
boxplot(wt~cyl,data=mtcars)
```

* Représenation de deux variables qualitatives par un diagramme bande:


```R
plot(cyl~vs,data=mtcars)
```


![png](output_19_0.png)


Pour chaque modalité du facteur explicatif ("vs") on la fréquence relative de chaque modalité du facteur à expliquer ("cyl") et la largeur de bande est proportionnelle à la fréquence de la modalité du facteur explicatif("vs").

* On peut **représenter une variable qualitative ("vs") en fonction d'une variable quantitative ("drat")**. La variable quantitative est découpée en classes selon la même méthode qu'un histogramme et dans chaque classe sont calculées les fréquences relatives de chaque modalité de la variable qualitative.


```R
plot(cyl~drat,data=mtcars)
```


![png](output_21_0.png)


Ce graphique peut être obtenu directement à partir de la fonction **splineplot**.


```R
spineplot(cyl~drat,data=mtcars)
```


```R
plot(mtcars[,"cyl"]~mtcars[,"drat"],xlab="Rear axle ratio",ylab="Number of cylinders")
```


![png](output_24_0.png)



```R
colorInd <-function(v,seuil)
{
  sapply(v,function(x) {if (x < seuil ) tabcol="black" else tabcol="red"})
}
 colS <- colorInd(mtcars$drat,3.5)
plot(mtcars[,"drat"], ylab="Rear axle ratio",cex=0.5, pch=16,col=colS)
```


![png](output_25_0.png)



```R
plot(mtcars[,"drat"], ylab="Rear axle ratio",cex=0.5, pch=16,col="red")
```


![png](output_26_0.png)



```R

```
