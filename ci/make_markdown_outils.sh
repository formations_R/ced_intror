#!bin/bash
# Environnement de départ
env 

# Construction des fichiers markdown à partir des notebooks
cd notebooks

## Introduction
jupyter nbconvert --to markdown IntroR.ipynb
ls -l
cp IntroR.md ../docs
cp -rf IntroR_files ../docs
ls -l ../docs

## Manipulation de données
jupyter nbconvert --to markdown ManipDon.ipynb
cp ManipDon.md ../docs
cp -rf ManipDon_files ../docs

## Objets R
jupyter nbconvert --to markdown Objets.ipynb
cp Objets.md ../docs
cp -rf Objets_files ../docs


## Graphiques
jupyter nbconvert --to markdown GraphiqueR.ipynb
cp GraphiqueR.md ../docs
cp -rf GraphiqueR_files ../docs

# Elements de programmation
jupyter nbconvert --to markdown ProgrammerR.ipynb
cp ProgrammerR.md ../docs
#cp -rf ProgrammerR_files ../docs

## Tutoriel ggplot2
cd Rgraphics
jupyter nbconvert --to markdown Rgraphics.ipynb
cp Rgraphics.md ../../docs
cp -rf Rgraphics_files ../../docs

# Les études
cd ../../TP/notebooks

## Les etudes
## -----------
# 
jupyter nbconvert --to markdown poidsTaille.ipynb
cp poidsTaille.md ../../docs/
#
# Concentrations moyenne pour l'année 2016 en polluant
jupyter nbconvert --to markdown ozoneClimat.ipynb
cp ozoneClimat.md ../../docs
#
jupyter nbconvert --to markdown mesureNO2_2016.ipynb
cp mesureNO2_2016.md ../../docs/
#jupyter nbconvert --to markdown mesureNO2_2016_Correct.ipynb
#cp mesureNO2_2016_Correct.md ../../docs/
#
jupyter nbconvert --to markdown ObjetManip.ipynb
cp ObjetManip.md ../../docs
# Traitement des dates
jupyter nbconvert --to markdown TraitementDate.ipynb
cp TraitementDate.md ../../docs/

## Les exercices
## --------------
jupyter nbconvert --to markdown Exercices.ipynb
cp Exercices.md ../../docs

# Recopie des enonces
cd ../enonces
cp intro-tp.pdf ../../docs/pdf
cp vecteur-tp.pdf ../../docs/pdf
cp matrices-tp.pdf ../../docs/pdf
cp Listes-tp.pdf ../../docs/pdf
cp fonctions-tp.pdf ../../docs/pdf
cp Strings-tp.pdf ../../docs/pdf
cp tableauDon-tp.pdf ../../docs/pdf
cp programmation-tp.pdf ../../docs/pdf
cp ImpExp-tp.pdf ../../docs/pdf
cp Visualisation-tp.pdf ../../docs/pdf


