## Présentation du cours

Ce cours s'adresse aux praticiens de la statistique, plus généralement aux personnes ayant à traiter des jeux de données dans des 
domaines variés (biologie, physique, sciences humaines,...) et ayant choisit R comme outil de traitement et d'analyse. Il a également 
pour objectif d'être un pré-requis à un cours avancé de développement avec R.

Le cours est centré principalement sur le logiciel R et son fonctionnement et même si on utilisera quelques procédures statistiques 
classiques au cours des manipulations, ça n'est pas un cours de statistiques.

Une **première partie** aborde les raisons du choix de ce logiciel pour le traitement des données, son installation, la documentation,
les packages, les interfaces utilisateurs disponibles (IDE) et une description de RStudio, l'IDE choisit pour ce cours. (IntroR.ipynb,IntroR.Rnw). 
On abordera quelques utilisations de bases.

Dans une **deuxième partie** on présente les concepts de bases : les objets, les fonctions,... (Objet.ipynb, Objet.Rnw).

Dans la **troisième partie**, on aborde les manipulations de données qui sont des opérations courantes dans la pratique statistique: 
importation de données, sauvegarde de données et de résultats, traitements des données manquantes, concaténation des niveaux de 
facteurs,... (ManipDon.ipynb, ManipDon.Rnw). On effectuera quelques traitements statistiques sur les données.

Le traitement statistique s'accompagne d'une visualisation claire et synthétique des résultats, nous décrivons dans la **quatrième partie**
les nombreuses possibilités offertes par R dans ce domaine (GraphiqueR.ipynb, GraphiqueR.Rnw).

Enfin dans la **cinquième partie**, nous présentons quelques éléments de programmation, comment construire un script R implémentant
un algorithme statistique ou une chaine de  traitements sur des données. On aborde dans cette partie les capacités de déboguage 
et de profiling de R et rapidement comment utiliser les ressources multi-coeur actuellement disponibles sur les portables ou 
serveurs à notre disposition (ProgrammerR.ipynb, ProgrammerR.Rnw).

Tout au long de ce cours on fera référence au package tidyverse. Le tidyverse est un ensemble d’extensions pour R construites autour d’une philosophie
commune facilitant l’utilisation de R dans les domaines les plus courants comme la manipulation des données, le recodage, la production de graphiques, etc.
( à consulter: Introduction à R et au tidyverse)

Pour chacune de ces parties, vous aurez des fichiers notebook que vous récupérerez sur le serveur GITLAB de GRICAD, un fichier
R notebook Markdown et une version *pdf.

[Pour en savoir plus sur les notebooks (jupyter, Rmarkdown)](https://www.datacamp.com/community/blog/jupyter-notebook-r#gs.6iMBYHw)

### Le serveur GITLAB de GRICAD
L'ensemble des consignes et des fichiers utiles à ce cours est stocké dans le projet [**CED-IntroR**](https://gricad-gitlab.univ-grenoble-alpes.fr/formations-statistiques-R/CED-IntroR) 
du serveur gitlab de GRICAD, vous avez accès à  ce serveur avec votre <identifiant,mot de passe\> AGALAN (voir menu **Consignes**)

### Le serveur de notebooks de GRICAD

  Connectez vous sur [le serveur de notebook de GRICAD](https://jupyterhub.u-ga.fr/) avec votre compte Agalan et utilisez les notebooks fournis
  (voir menu **Consignes**).

### Installation de R sur votre portable

Pour un meilleur suivi du cours, nous vous conseillons d'installer R (Version 3.4.x au moins) et [RStudio](https://www.rstudio.com/products/RStudio/) 
sur votre ordinateur personnel , de préférence avant la première séance (voir **Informations Pratiques -> Consignes**).

Il s’agit de deux logiciels libres, gratuits, téléchargeables en ligne et fonctionnant sous Windows, Mac-OS et Linux.

Pour installer R, il suffit de se rendre sur une des pages suivantes 2 :

   * [Installer R sous Windows](https://cloud.r-project.org/bin/windows/base)

   * [Installer R sous Mac](https://cloud.r-project.org/bin/macosx)

[RStudio](https://www.rstudio.com/products/RStudio/) est un environnement de développement intégré, qui propose des outils et 
facilite l’écriture de scripts et l’usage de R au quotidien. C’est une interface bien supérieure à celles fournies par défaut 
lorsqu’on installe R sous Windows ou sous Mac-OS

[Installer RStudio](https://www.rstudio.com/products/rstudio/download/#download), téléchargez la version adaptée à votre système :

Nous verrons comment installer de nouveaux package.

### Travailler sur des données personnelles

Des exemples simples vous seront fournis pour faire quelques exercices, il est toujours plus efficace de s'éxercer sur des 
données personnelles. Ces données auront déjà été nettoyées en partie pour éviter de perdre du temps sur de la validation de 
saisie.




