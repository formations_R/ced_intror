
# Programmer avec R

La programmation dans R est basée sur des principes identiques à d'autres logiciels de calcul 
scientifique, on y retrouve les structures classiques comme les blocs d'instructions, les boucles, les expressions conditionnelles et des fonctions prédéfinies utiles au calcul et à la pratique statistiques.

## Expressions et blocs d'expressions

Un **bloc d'expressions** est une suite d'expressions constituées d'opérateurs et
d'autres objets (variables, constantes,fonctions) encadrées par des
accolades(**{}**).

* Toutes les expressions d'un bloc sont **évaluées les unes à la suite des autres**.

* Tous les assignements de variables seront effectifs.

* **Le bloc entier** est lui-même **une expression** dont la valeur sera la dernière expression évaluée dans le bloc.


```R
monbloc <- { a <- 1:10
 somme <- sum(a)}
a
somme
monbloc
```

## Structure de contrôle

### Les boucles for ou while

Les boucles classiques de la programmation sont disponibles sous **R**.

* On veut afficher tous les entiers de 1 à 20. Une solution est d'utiler une boucle for.


```R
for (i in 1:20) print(i)
```

L'ensemble des valeurs prise par l'**indice de la boucle** i est un vecteur qui peut être de mode divers obtenu à partir des constructeurs classiques de vecteurs dans R (c(), seq(), rep(),...).


```R
# 
for (i in seq(1,20,2)) print(i)
```


```R
# Indice de mode "character"
vecteur <-c("Alice","Marcel","Alexis")
for(i in vecteur) print(i)
```

* En général, on a plusieurs instructions à effectuer à chaque itération de la boucle for. On encadre ces itérations avec des accolades "**{}**".

Un boucle s'écrit:

**for** (i in vecteur) **{**<br\>
\+  instruction1 <br\>
\+  instruction2 <br\>
\+  instruction3 <br\>
\+  ... <br\>
**}**



```R
# Exemple boucle for
meanx <- 0
meany <- 0
n <- 1000
z <- rep(0,n)
x <- rnorm(n,0,1)
y <- runif(n)
for (i in 1:n){
    meanx <- meanx + x[i]
    meany <- meany + y[i]
    z[i] <- x[i] + y[i]
}
meanx <- meanx/n
meany <- meany/n
```

* On peut sauter une itération dans une boucle avec l'instruction **next**.

Pour mettre à zéro tous les éléments d'une matrice sauf les éléments diagonaux :


```R
mat <- matrix(1:9,ncol=3,nrow=3)
mat
for(i in 1:3){
 for(j in 1:3){
  if(i == j) next
  mat[i,j] <- 0
  }
 }
mat
```

* L'instruction **while** qui exécute un bloc d'instruction tant qu'une condition est vraie

**while** (condition) **{**<br\>
      \+ expression1<br\>
   \+ expression2<br\>
   \+ ...<br\>
   \+**}**


```R
i <- 1
while (i<3){
    print(i)
    i <- i+1
}
```

**Exercices**:

1.

2.


* Une autre possibilité est la boucle **repeat**. La sortie de la boucle est assurée par l'ordre **break**

### Les expressions conditionnelles (if et else)

* Un bloc d'instructions est exécuté si et seulement si une condition est vraie.

**if** (condition) **{** <br\>
  \+ expression1 <br\>
  \+ expression2 <br\>
  \+ ... <br\>
  \+ **}**
  
Par exemple, on utilise la boucle **repeat** pour imprimer les entiers de 1 à 3 exclu.
  


```R
i <- 1
repeat {
    print(i)
    i <- i +1
    if (i >= 3) break
}
```

* Une autre condition peut être ajoutée à la suite du if, la condition **else**

**if** (condition) **{** <br\>
  \+ expression1 <br\>
  \+ expression2 <br\>
  \+ ... <br\>
  \+ **} else { **<br\>
  \+ expression3 <br\>
  \+ expression4 <br\>
  \+ ... <br\>
  \+ } 
  
  **Attention!!!** l'ordre **else** doit être sur la même ligne qur l'accolade fermante "}"


```R
x <- 7
if( x %% 2 == 0){
parit <- "pair"
} else {
parit = "impair"
}
parit
```

* Une version **vectorisée** 

**ifelse**(test, oui, non) <br\>
  - *test*: expression logique<br\>
  - oui: valeur si test est vraie<br\>
  - non: valeur si test est faux
  


```R
x <- rnorm(10)
x
ifelse(x > 0, "positif", "negatif")
```

### Alternative à la boucle for sous R

Ce type d'alternative est en général moins couteuse en temps.

*Exemple*: remplacer toutes les valeurs négatives d'un vecteur par -1.

  - **La solution classique**:




```R
x <- rnorm(10)
x
for(i in 1:length(x)){
if(x[i] < 0) x[i] <- -1
}
x
```

   - **l'approche sous R**


```R
x[x < 0] <- -1
x
```

## Les fonctions prédéfinies
Les boucles implicites (apply,lapply,...) ont été prédéfinies dans R pour éviter d'avoir recours à des boucles généralement coûteuses en temps de calcul.

### La fonction apply 

* La fonction apply permet d'appliquer une même fonction à toutes les marges d'un tableau.

**apply** <- function(X, MARGIN,FUN,...)

*Exemple*: calculer la moyenne de toutes les colonnes d'une matrice et de toutes les lignes:



```R
mat <- matrix(1:12, 3, 4)
# moyenne des lignes
apply(mat,MARGIN=1,mean) 
# moyenne des colonnes
apply(mat,MARGIN=2,mean) 

```

Dans cet exemple, R fournit deux fonctions qui font les mêmes calculs: **colMeans** et **rowMeans**


```R
rowMeans(mat)
colMeans(mat)
```

* La fonction **apply** peut être utilisée sur un **tableau à trois dimensions**.

Si le tableau a trois dimension, on peut exécuter la fonction:

    - par ligne (MARGIN=1), 
    - par colonne (MARGIN=2), 
    - par profondeur (MARGIN=3),
    - pour des croisements ligne-colonne (MARGIN=c(1,2)),
    - pour des croisements ligne-profondeur (MARGIN=c(1,3)),
    - pour des croisements colonne-profondeur (MARGIN=c(2,3))
  
*Exemple*: calcul de la somme d'un tableau à trois dimensions par couple ligne-colonne:


```R
set.seed(1234)
Y <- array(sample(24),dim=c(4,3,2))
Y
apply(Y,MARGIN(c(1,2)),FUN=sum,na.rm=TRUE)
```

* La fonction **apply** peut être utilisée avec une fonction utilisateur


```R
maFonction <- function(x,y){
    z <- x +sqrt(y)
    return(1/z)
}
set.seed(1234)
X <- matrix(sample(20),ncol=4)
X
apply(X,MARGIN=c(1,2),FUN=maFonction,y=2)
```

*Exemple*: remplacer les données manquantes d'un dataset.

On considère le jeu de données **airquality** (dataset). 


```R
data(airquality)
head(airquality)
```

Il y a des **données manquantes**, le choix est fait de remplacer les valeurs manquantes par **la moyenne de la variable**.

    - Utilisation de **boucles for**


```R
for (i in 1:nrow(airquality)) {
  for (j in 1:ncol(airquality)) {
      if (is.na(airquality[i, j])) {
          airquality[i, j] <- mean(airquality[, j],
                                   na.rm = TRUE)
      }
  }
}
head(airquality)
```

    - Utilisation de la fonction **apply** et la version vectorisée **ifelse**


```R
head(apply(airquality, 2, function(x) ifelse(is.na(x),mean(x, na.rm = TRUE), x)))
 
```

### Les fonctions lapply() et sapply() permettent d'appliquer une fonction à tous les éléments d'une liste ou d'un vecteur

* **lapply()** fournit une **list** en sortie


```R
a <- seq(1,8,by=2)
maliste <- as.list(a)
maliste
f <- function(x) x^2
b <- lapply(maliste, f)
b
class(b)

```

* **sapply()** fait la même chose et retourne un résultat de type **vecteur** :


```R
c <-sapply(maliste, f)
c
class(c)
```

### La fonction tapply()

* La fonction **tapply()** permet d'appliquer une fonction **à des groupes** définis par une **variable qualitative**.

*Exemple*: Considérons le dataset **iris** fournit par R. Calculons la moyenne de la variable quantitative **Sepal.Length** sur chaque groupe défini par la variable qualitative **Species**.


```R
data("iris")
summary(iris)
# Moyenne par groupe defini par la variable Species
tapply(iris$Sepal.Length, iris$Species, mean)
```

### La fonction aggregate 

La fonction **aggregate()** travaille sur des **data-frame**, elle sépare des données en sous-groupe, définis à partir d'un vecteur, et calcule une statistique sur l'ensemble des variables du data-frame sur chaque sous-groupe.


```R
# Deux variables quantitatives Z et T
Z <- 1:5
T <- 5:1
# Deux variables qualitatives
vec1 <- c(rep("A1",2),rep("A2",2),rep("A3",1))
vec2 <- c(rep("B1",3),rep("B2",2))
# Construction du data-frame
df <- data.frame(Z,T,vec1,vec2)
df
# Moyenne de Z et T sur les groupes définis par la variable "vec1"
aggregate(df[,1:2],list(FacteurA=vec1),sum)
# Moyenne de Z et T sur les groupes définis par les variables "vec1" et "vec2"
aggregate(df[,1:2],list(FacteurA=vec1,FacteurB=vec2),sum)
```

### La fonction sweep

La fonction **sweep()** permet d'appliquer une procédure à toutes les marges d'un tableau.

Si on veut **centrer** et **réduire** les colonnes d'une matrice.


```R
set.seed(1234)
X <- matrix(sample(12),ncol=3)
X
mean.X <- apply(X,2,mean)
"moyenne des colonnes"
mean.X
sd.X <- apply(X,2,sd)
"ecart-type des colonnes"
sd.X
# centrer les données
Xc <- sweep(X,2,mean.X,FUN="-")
Xc
# Moyenne de chaque colonne de Xc
apply(Xc,2,mean)
# Reduire les données
Xcr <-sweep(Xc,2,sd.X,FUN="/")
# Ecart-type de chaque colonne de Xcr
apply(Xcr,2,sd)
Xcr
```

* Pour centrer et réduire une matrice, on peut simplement utiliser la fonction **scale()**


```R
Xscale <-scale(X,center=TRUE,scale=TRUE)
apply(Xscale,2,mean)
apply(Xscale,2,sd)
Xscale
```

### La fonction by() 

La fonction **by()** permet d'appliquer une même fonction à un data-frame pour les différents niveaux d'un facteur ou d'une liste de facteurs, elle est similaire à la fonction **tapply()**.


```R
set.seed(1234)
T <- rnorm(100)
Z <- rnorm(100) +3*T+5
# le facteur:vec1
vec1 <- c(rep("A1",25),rep("A2",25),rep("A3",50))
# data-frame: don
don <- data.frame(Z,T)
# Calcul par groupe
by(don,list(FacteurA=vec1),summary)
```

* La fonction **by()** avec une fonction utilisateur.


```R
mafonction <- function(x){
    summary(lm(Z~T),data=x)$coef
}
# 
by(don,vec1,mafonction)
```

### La fonction replicate

La fonction **replicate()** permet de répéter une expression n fois.


```R
set.seed(1234)
replicate(n=8,mean(rnorm(100)))
```


<ol class=list-inline>
	<li>-0.156761742442039</li>
	<li>0.0412431799463585</li>
	<li>0.154603672070928</li>
	<li>-0.00810513624308551</li>
	<li>-0.021785870260462</li>
	<li>-0.136877005738466</li>
	<li>-0.0878617963239032</li>
	<li>-0.000837192642517517</li>
</ol>



### La fonction outer

La fonction outer permet de répéter une fonction sur chaque combinaison de deux vecteurs.


```R
Mois <- c("Jan","Fév","Mars")
Annee <- c(2008:2010)
outer(Mois,Annee,"paste")
outer(Mois,Annee,"paste",sep="_")
```


<table>
<tbody>
	<tr><td>Jan 2008 </td><td>Jan 2009 </td><td>Jan 2010 </td></tr>
	<tr><td>Fév 2008 </td><td>Fév 2009 </td><td>Fév 2010 </td></tr>
	<tr><td>Mars 2008</td><td>Mars 2009</td><td>Mars 2010</td></tr>
</tbody>
</table>




<table>
<tbody>
	<tr><td>Jan_2008 </td><td>Jan_2009 </td><td>Jan_2010 </td></tr>
	<tr><td>Fév_2008 </td><td>Fév_2009 </td><td>Fév_2010 </td></tr>
	<tr><td>Mars_2008</td><td>Mars_2009</td><td>Mars_2010</td></tr>
</tbody>
</table>



### Pour en savoir plus

[Introduction à R et au tidyverse](https://juba.github.io/tidyverse/index.html)


