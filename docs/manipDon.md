
# Manipuler des données dans R

En statistique, les données constituent le point de départ de toute analyse, un premier travail de mise en forme de ces données est presque toujours indispensable. Il faudra savoir maitriser des opérations comme:

* Importation de données sous différents formats, 
* exporter des données et des résultats sous différents formats,
* concaténer ou extraire des données, 
* repérer les individus ayant des données manquantes ou aberrantes.
* changer le type de certaines variables pour les adapter aux traitements envisagés,
* $\ldots$

R fournit des outils et des capacités de programmation pour effectuer ces différentes tâches.

## Importer des données

Les données sont initialement collectées, éventuellement prétraitées par un logiciel. 

Chaque logiciel ayant son propre format de stockage, le plus simple est souvent d'échanger les données par un format commun à tous, **le format texte** ( .csv par exemple).

On peut également utiliser **les formats propriétaires** des autres logiciels en utilisant un package adapté (le package foreign par exemple), le choix dépendant du contexte et du volume des données.

### Cas des fichiers **csv** 

Les avantages des fichiers **csv**:

  - Peut être lu par n'importe quel logiciel passé, présent et probablement futur,
  - Pour la compatibilité entre plate-forme (Windows, Mac, Linux),
  - Pour la facilité de lecture par un être humain comparativement à d'autres formats tels que XML, HL7, JSON etc.

**Mais** pas forcément adapté aux gros volumes de données pour son volume de stockage et la rapidité de lecture.

R lit des données en format texte avec les fonctions **read.table()**,**read.csv()**,**scan()**,**read.fwf()**,$\ldots$

* Le fichier "donnees.csv" est stocké dans le répertoire data sitée dans le répertoire de travail 
    
    - pour connaître le répertoire de travail, utiliser la fonction **getwd()**
    - pour définir le répertoire de travail, la fonction **setwd()**
    
 Le résultat de la fonction **read.table** ou **read.csv** est de type **data-frame**.


```R
# Lecture du fichier donnees.csv
getwd() # repertoire de travail
don <- read.csv(file = "data/donnees.csv",header=TRUE,sep=";",dec=",",row.names=1)
summary(don)
class(don)
```


'/home/viryl/notebooks/CED-IntroR/notebooks'



         taille          poids          pointure    sexe 
     Min.   :158.0   Min.   :72.00   Min.   :42.0   M:3  
     1st Qu.:166.8   1st Qu.:75.00   1st Qu.:42.5        
     Median :175.5   Median :78.00   Median :43.0        
     Mean   :172.5   Mean   :76.67   Mean   :43.0        
     3rd Qu.:179.8   3rd Qu.:79.00   3rd Qu.:43.5        
     Max.   :184.0   Max.   :80.00   Max.   :44.0        



'data.frame'


   - l'argument **sep** : indique que les valeurs sont séparées par **";"** (**" "** pour un espace, **"\t"** pour une tabulation)
   - l'argument **dep** : indique que le séparateur de décimal est **","**
   - l'argument **header**: indique si la première ligne contient les noms des variables (TRUE) ou non(FALSE).
   - l'argument **row.names** : indique que la colonne 1 n'est pas une variable mais l'identifiant des individus.
   

* Un caractère spécial peut indiquer qu'il y a des données manquantes:

Le fichier **don2.csv** contient des données manquantes codées **"\*\*\*"**, on ajoute l'argument **na.strings**


```R
don2 <- read.csv(file = "data/don2.csv",header=TRUE,sep=";",dec=",",row.names=1,na.strings="***")
summary(don2)
mode(don2)
class(don2)
mean(don2$poids,na.rm=TRUE)
```

* Le chemin peut-être une URL:


```R
decath <- read.table("http://www.agrocampus-ouest.fr/math/livreR/decathlon.csv",header=TRUE,sep=";",row.names=1)
summary(decath)
```

### Fonctions utiles dans un data-frame

* **head()** - pour voir les 6 premières lignes 
* **tail()** - pour voir les 6 dernières lignes 
* **dim()** - ses dimensions 
* **nrow()** - le nombre de lignes 
* **ncol()** - le nombre de colonnes 
* **str()** - structure de chaque colonne
* **names()** - liste l'attribut **names** d'un data.frame (ou n'importe quel autre objet), les noms des colonnes
* **dimnanes()** - liste l'attribut **row.names** d'un data.frame.

### La fonction **scan**

La fonction **scan** est plus flexible que **read.table**.

* Une différence est qu'il est possible de spécifier le mode des variables:


```R
help(scan)
```


```R
mydata <- scan("data/don.txt",skip=1,what = list("", 0, 0,0))
class(mydata)
mydata[[1]] # premiere variable
mydata[[1]][1] # 
```

Dans cet exemple, scan lit 4 variables, la première de mode caractère et les trois suivantes sont de mode numérique.

**myDat** est une liste de 4 vecteurs.

* scan() peut être utilisée pour créer des objets de mode différent (vecteurs, matrices, tableaux de données, listes,...).
*
* Par défaut, c'est-à-dire si what est omis, scan() crée un vecteur numérique.

Pour en savoir plus **help(scan)**

### Les formats propriétaires

R peut également lire des fichiers dans d'autres formats (**Excel, SAS, SPSS**,$\ldots$) et accéder à des **bases de données**.

* Le package **foreign** permet d'importer des données en format propriétaire binaire tels que **Stata, SAS, SPSS, etc**.

  **library**(foreign) <br\>
  **read.dta**("calf pneu.dta") # for Stata files<br\>
  **read.xport**("file.xpt") # for SAS XPORT format<br\>
  **read.spss**("file.sav") # for SPSS format<br\>
  **read.mpt**("file.mtp") # for Minitab Portable Worksheet<br\>

Une autre solution pour des **fichiers SPSS**, le package **Hmisc**<br\>

### Base de données relationnelles et autres formats

Certains packages permettent de se connecter directement sur des bases de données de type **MySQL** ou **PostgreSQL**
(MongoDB,Redis,ousqlite,...). Dans ce cas,le mode d’interaction avec les données est légèrement différent
 car on utilise alors le langage de requête propre au langage, à moins d’utiliser des packages qui permettent d’assurer
 la conversion à partir des commandes R habituelles telles que **subset()**.

Pour en savoir plus : 

* [Cookbook for R](http://www.cookbook-r.com/Manipulating_data/)

## Sauvegarder des données ou des résultats

Une fois les analyses effectuées et les résultats obtenus, il est souvent important de les sauvegarder pour les communiquer à d'autres personnes ou d'autres logiciels ou les réutiliser dans d'autres analyses.

### En format texte

Le format texte est fréquemment le format utilisé en utilisant la fonction **write.table()**.


```R
tablo_res <- matrix(runif(40), ncol=4)
# sauvegarde les resulatts dans le repertoire resultat
setwd("/home/viryl/notebooks/CED-IntroR/resultat")
write.table(tablo_res,"monfichier.csv",sep=";",row.names=FALSE)
getwd()
```

### En format binaire RData

* Exporter les résultats et/ou les données dans un fichier binaire suffixé
**.Rdata** que R sera capable de décrypter par la suite grâce à la fonction **save()**

   **save(file="nom du fichier",liste des variables)** (help(save))
   


```R
x <- runif(20)
y <- list(a = 1, b = TRUE, c = "oops")
getwd()
save(x, y, file = "xy.RData")
```

* L'utilisation des fichiers sauvegardées par la commande **save()** se fait par la commande **load()**.


```R
rm(list=ls())
"avant load"
ls()
load(file="xy.RData")
"après load"
ls()
```

## Manipuler des variables

* **Changer le type d'une variable**: à l'issue d'une importation, une variable qualitative dont les modalitées sont numériques sera comprise par R comme une variable quantitative,$\ldots$

* **Découper en classes une variable quantitative**: le passage d'une variable quantitative à une variable qualitative est fréquemment nécessaire en statistiques pour l'adapter à la méthode utilisée (AFC,AFCM,$\ldots$)

* **Modifier les niveaux d'une variable qualitative**: fusionner un ou plusieurs niveaux en fonction des effectifs,$\ldots$

* **Repérer les données manquantes**: permettre la prise en compte des données manquantes dans le traitement statistique des données.

* **Repérer les données aberrantes**: permettre la prise en compte des données aberrantes dans le traitement statistique des données.

* $\ldots$

### Quantitatives ou qualitative?

 Trois méthodes:
 


```R
# on genere une variable X
X <-c(rep(5,2),rep(12,4),13)
X
# Une variable qualitative?
Xq <is.factor(X)
# Une variable quantitative
is.numeric(X)
# Statistques descriptives
summary(X)
```

Les trois instructions nous indiquent que X est une variable quantitative.

### Variable qualitative vers variable quantitative
* Avec recodification des modalités: 1 seule étape

Par défaut, la conversion d'un facteur en variable quantitative, utilise la recodification des modalités à **1 to nlevels(x)**


```R
Xqual <- factor(c(rep(5,2),seq(0,6,by=2),13))
Xqual
nlevels(Xqual)
summary(Xqual)
is.factor(Xqual)
as.numeric(Xqual)
```

* Sans recodification des modalités

Si on veut éviter la recofication, lorsque la valeur des modalités a un sens de quantité, il sera nécessaire de faire une première conversion de mode en mode **"character"**, avant la conversion en **"numeric"**.


```R
Xqual
prov <- as.character(Xqual)
prov
Xquant <- as.numeric(prov) # as.numeric(as.character(Xqual))
Xquant
summary(Xquant)
```

### Variable quantitative vers variable qualitative
* En conservant les valeurs de la variable quantitative


```R
rm(list=ls())
ls()
X <- seq(0,10,2)
X
summary(X)
Xqual <- as.factor(X)
Xqual
summary(Xqual)
```

### Découpage en classes d'une variable quantitative

Le découpage en classes d'une variable quantitatives peut se faire avec **deux approches**:

* Les **seuils des classes** sont choisis par les utilisateurs: pour définir ces seuils de façon automatique, on utilisera la fonction **cut**. Les bornes des intervals sont fournies à la fonction cut, les classes sont de la forme $\bf{] a_i,a_{i+1}]}$.


```R
set.seed(654) # on fixe la graine du generateur
X <- rnorm(n=100,mean=0,sd=1)
# Decoupage en 3 niveaux: [min(X),-0.2],[-0.2,0.2],[0.2,max(X)]
Xqual <- cut(X,breaks = c(min(X)-1e-10,-0.2,0.2,max(X)))
class(Xqual)
table(Xqual)
summary(Xqual) # ou table(Xqual)
```

On indique "$min(X)-1e-10$" pour que le minimum appartienne à la classe.

* Un découpage automatique proposant **des effectifs équivalents dans chaque classes**: si nous voulons des effectifs équilibrés dans chacune des trois modalités, on utilisera la **fonction quantile**.


```R
decoupe <- quantile(X,probs=seq(0,1,length=4)) 
decoupe
decoupe[1] <- decoupe[1]-1e-10
Xqual <- cut(X,decoupe)
table(Xqual)
```

### Modifier les modalités d'un facteur

* Modifier les labels des modalités


```R
Xqual <- cut(X,decoupe)
levels(Xqual)
#Xqual
table(Xqual)
#
levels(Xqual) <- c(1,2,3) # modification les labels des modalités
#Xqual
table(Xqual)
```

* Fusionner un ou plusieurs niveaux


```R
levels(Xqual)
levels(Xqual) <- c(1,2,1)
levels(Xqual)
table(Xqual)
```

* Définir un niveau de référence: pour certaines méthodes, il faudra tenir compte de l'ordre d'apparition des niveaux ou spécifier une niveau de référence (analyse de variance,$\ldots$), on utilise la fonction **relevel**:


```R
X <- c(1,2,1,3,2,2,1)
Xqual <- factor(X,label=c("classique","nouveau","placebo"))
levels(Xqual)
Xqual2 <- relevel(Xqual,ref="placebo")
levels(Xqual2)
```

* Contrôler l'ordre des niveaux: recréer un facteur à partir du facteur existant en spécifiant l'ordre des niveaux.


```R
table(Xqual)
Xqual3 <- factor(Xqual,levels=c("placebo", "nouveau","classique"))
Xqual3 <- Xqual3[-4] # elimine l'individu avec la modalite "3"
#Xqual3
table(Xqual3) # la modalite "3" n'apparait plus
# elimine la modalite "3"
Xqual3 <- factor(as.character(Xqual3)) # elimine la modalite "3"
table(Xqual3)
```

## Manipuler des individus

### Repérer les individus manquants 
Dans R, les données manquantes sont représentées par **NA**. La fonction **is.na**
permet de les retrouver.

* Dans une variable:


```R
X <- rnorm(10,0,1)
X[c(2,7,10)] <- NA
summary(X)
mean(X)
mean(X,na.rm=TRUE)
#
selectNA <- is.na(X)
selectNA
which(selectNA) # Quels sont les indices correspondants
# "!" : negation 
X2 <-X[!selectNA] # On élimine les individus correspondants
```

* Dans sun tableau de données:


```R
Y <- factor(c(rep("A",3),NA,rep("M",4),"D",NA))
# data-frame avec les deux variables X et Y
don <-data.frame(X,Y)
summary(don)
# 
selectNA <- is.na(don)
#mode(selectNA)
class(selectNA)
#selectNA
#
# au moins une donnee manquante pour un individu
aelim_any <- apply(selectNA, MARGIN=1,FUN=any)
aelim_any 
# Toutes les donnees sont manquantes pour un individu
aelim_all <- apply(selectNA, MARGIN=1,FUN=all)
aelim_all
```


```R
don2 <-don[!aelim_all,] # individus élimines
which(is.na(don))
which(is.na(don),arr.ind=T) # option arr.ind (array indices) de which
```

### Repérer les individus aberrants

On utilise la fonction boxplot qui fournit dans sa composante out les valeurs
aberrantes.

Dans l'exemple, on utilise le package "rpart", vous pouvez l'installer en local, voir ~/Utils/install_local_package.ipynb


```R
rm(list=ls())
```


```R
library(rpart)
data("kyphosis")
names(kyphosis)
boxNumber <- boxplot(kyphosis[,"Number"]) # repere
```


```R
class(boxNumber)
attributes(boxNumber)
```


```R
# les individus aberrants
valaberrante <- boxNumber$out
#kyphosis[,"Number"]%in%valaberrante
which(kyphosis[,"Number"]%in%valaberrante)
```

## Concaténer des tableaux de données

Pour regrouper deux tableaux peut être vu de deux façons:

* Aggréger des individus sur lesquels ont été observées les mêmes variables en concaténant des tableaux de données l'un en dessous de l'autre avec la fonction **rbind**.


```R
# exemple rbind sur des matrices avec le même nombre de collonnes
X <- matrix(21:24,ncol=2)
colnames(X) <- paste("X",1:2,sep="")
rownames(X) <- paste("individu",1:2,sep="")
X
Y <- matrix(11:14,ncol=2)
colnames(Y) <- paste("Y",1:2,sep="")
rownames(Y) <- paste("individu",3:4,sep="")
Y
Z <-rbind(X,Y)
Z
class(Z)
```

Les deux matrices **X**et **Y** n'ont pas les mêmes noms de collonne, la matrice issue de la concaténation prend les noms de la première matrice.

* pour la concaténation des data-frames, il sera nécessaire que les deux data-frame aient les mêmes noms de variables. dans la cas contraire, il sera nécessaire de renommer les variables d'un data-frame.


```R
Xd <- data.frame(X)
Yd <- data.frame(Y)
Zd <- rbind(Xd,Yd)
```


```R
colnames(Yd) <- colnames(Xd)
Zd <- rbind(Xd,Yd)
Zd
class(Zd)
```

* Aggréger des variables qui ont été observées sur les même individus en concaténant des tableaux de données l'un à coté de l'autre avec la fonction **cbind()**.


```R
Xd <- data.frame(matrix(21:24,ncol=2))
colnames(Xd) <- paste("X",1:2,sep="")
rownames(Xd) <- paste("individu",1:2,sep="")
Xd
Yd <- data.frame(matrix(11:14,ncol=2))
colnames(Yd) <- paste("Y",1:2,sep="")
rownames(Yd) <- paste("individu",3:4,sep="")
Yd
Wd <-cbind(Xd,Yd)
Wd

```

La fonction **cbind()** ne vérifie pas le nom des lignes, les noms des lignes du premier data-frame sont conservés. 

* Il est possible de fusionner deux tableaux selon une **clef** avec la fonction **merge**.

Concaténons deux tableaux de données:
  - le premier tableau regroupe une variable continue (age) et deux variables qualitatives ("prenom" , "ville").


```R
age <- c(45,32,67)
ville <- factor(c("rennes","rennes","marseille"))
prenom <- c("Alice","Marcel","Alexis")
indiv <- cbind.data.frame(age, prenom, ville)
class(indiv)
indiv
```

  - le second tableau regroupe les caractéristiques des villes.


```R
population <- c(300,500,600)
caractVille <- cbind.data.frame(ville =c("rennes","lyon","marseille"),pop=population)
caractVille
```

On fusionne les tableaux en un seul où seront répétées les caractéristiques des villes à chaque ligne du tableau. On effectue une fusion avec la fonction **merge()** et la clef **ville**.


```R
merge(indiv,caractVille,by="ville")
```

*exercices*: ~/CED-IntroR/TP/enonces/ImportFusion.pdf

## Tableaux croisés

Lorsqu'on a deux variables qualitatives observées sur un échantillon, les données
peuvent être présentées sous deux formes:

### Tableau de contingences


```R
tension <- factor(c(rep("Faible",5),rep("Forte",5)))
tension
laine <- factor(c(rep("Mer",3),rep("Ang",3),rep("Tex",4)))
laine
# fusionnons ces deux variables dans un data.frame
don <-data.frame(tension, laine) # cbind.data.frame(tension,laine)
# Tableau de contingences
tabcroise <-table(don$tension,don$laine)
class(tabcroise)
tabcroise
```

### Tableaux Individus X Variables


```R
tabframe <- as.data.frame(tabcroise)
tabframe
```

Nous obtenons une fréquence pour chaque combinaison et non pas une
ligne par individu. (pas très compliqué à obtenir, voir exercice indVarTab.ipynb)

Pour en savoir plus:

* [Gestion des données avec R](https://www.fun-mooc.fr/c4x/UPSUD/42001S03/asset/data-management.html) (Christophe Lalanne & Bruno Falissard -MOOC "Introduction à la statistique avec R").

* [Begin'R (Bordeaux INP](http://beginr.u-bordeaux.fr/index.html#sommaire)

* [Cookbook for R](http://www.cookbook-r.com/Manipulating_data/)

* [Introduction à R et au tidyverse](https://juba.github.io/tidyverse/index.html)


```R

```
