



## Etude de la polution de l'air en fonction de données climatiques

(*Cet exemple est tiré du livre "Statistiques avec R" de Pierre-André Cornillon (Presse Universitaire de Rennes), il peut être intéressant pour ceux qui sont intéressés par l'interprétation des résultats de le consulter*).

La pollution de l'air est actuellement une des préoccupations majeures de santé publique. Des associations de surveillance de la qualité de l'air existent sur tout le territoire français et mesurent la **concentration des polluants** ainsi que **les conditions météorologiques** comme la température, la nébulosité, le vent,$\ldots$

On dispose de 112 données relevées durant l'été 2001 à Rennes stockées dans le fichier texte **"ozone.txt"** dans le répertoire **".../CED-IntroR/TP/data"**.

### Relation concentration en ozone et température

Analyse de la relation entre le maximum journalier de la concentration en ozone (en $\mu/m^3$) et la température par une méthode de régression linéraire simple, fonction **lm**(linear model) de R.

**Les différentes étapes**

#### Importer les données et extraire les variables d'intérêts ("max03" et "T12").


```R
# Lecture des données
setwd('/home/viryl/notebooks/CED-IntroR/TP/data')
...
names(ozone)
# Extraction des données utiles
ozone1 <- 
names(ozone1)
```

#### Faire un premier traitements descriptifs de ces variables.


```R
# Statistiques descriptives des données de l'étude
...
```

#### Analyse du lien entre ces deux variables par une représentation graphique.

On représente le nuage de points (maxO3,T12), chaque point preprésente, pour un jour donné, une mesure de la température à 12h et le pic d'ozone de la journée.


```R
# Nuage de point ("maxO3","T12")
...
```

#### Estimation des paramètres du modèle de régression simple.<br\>

La fonction **lm** (linear model) permet d'ajuster un modèle linéaire. 

  - On observe les composantes du résultat obtenu en sortie de la fonction **lm** (fonction **names**).
  - Les fonctions **summary** et **anova** sont utilisées pour obtenir et afficher les paramètres permettant de faire une analyse des résultats.
  


```R
help(lm)
```


```R
reg.simple <- 
names(reg.simple)

class(reg.simple)
...
```

#### Tracer la droite de régression sur le nuage de point obtenue en 3.

  - en utilisant simplement la fonction **abline** sur le résultat obtenu.
  - ou la fonction **line()** sur le domaine d'observation de la variable explicative T12.


```R
# Utilisation de la fonction abline
...
```


```R
plot(maxO3~T12,data=ozone1,pch=15,cex=0.5)
grillex <- ...
grilley <- ...
...
```

#### Analyser les résidus

On peut ajouter au jeu de données initial, les valeurs ajustées, les résidus, les valeurs estimées...On pourra ensuite construire des graphes en utilisant ces nouvelles variables.

#### Estimer "maxo3" pour une nouvelle valeur de "T12"

Pour prédire "maxO3" pour une nouvelle vauleur de "T12", il suffit d'utiliser les estimations. La valeur prédite aura un intérêt si elle est donnée avec son intervalle de confiance, on utilisera de la fonction **predict** qui prend pour argument, le résultat de la régression et la ou les nouvelles valeurs sous forme d'un data-frame.

On dispose d'une nouvelle valeur de T12 égale à 19 degrés pour le $1^{er}$ octobre.


```R
xnew <- 19
...
```

Pour représenter sur un même graphe l'intervalle de confiance d'une valeur lissée et l'intervalle de confiance d'une valeur prédite, nous calculons ces intervales sur l'ensemble des points ayant servi à la construction de la droite de régression.


```R
# Données explicatives
grillex.df <- as.data.frame(grillex)
dimnames(grillex.df)[[2]] <- "T12"
ICdte <- predict(reg.simple,new=grillex.df,interval="conf",level=0.95)
ICprev <- predict(reg.simple,new=grillex.df,interval="pred",level=0.95)
plot(maxO3~T12,data=ozone1,pch=15,cex=0.5)
matlines(grillex,cbind(ICdte,ICprev),lty=c(1,2,2,3,3),col=1)
legend("topleft",lty=2:3,c("prev","conf"))
```

### Regression multiple

La régression linéaire multiple permet d'expliquer et/ou prédire une variable quantitative par p variables quantitatives, c'est une généralisation du modèle de régression simple.

On revient à notre problème et on veut analyser la relation entre le maximum journalier de la concentration en ozone et la température à différentes heures de la journée, la nébulosité à différentes heures de la journée, la projection du vent sur l'axe Est-Ouest à différentes heures de la journée et la concentration maximale de la veille du jour considéré.

** Les diférentes étapes **

#### Importer les données et conserver les données utiles.


```R
# Lecture des données
names(ozone)
# Extraction des données utiles
ozone2 <- ...
names(ozone2)
summary(ozone2)
```

#### Représenter les variables

Pour valider les données, on effectue une analyse univariée des variables(statistiques descriptives, histogramme,...)

Le nombre de variables n'étant pas très élevé, on peut représenter les variables deux par deux sur un même graphique (fonction **pairs**). On pourrait également explorer les données avec une ACP (package FactoMineR, FactoShiny).


```R
# plot 

```

#### Estimer les paramètres

Pour estimer les paramètres, il faut écrire le modèle. On utilise la fonction **lm** avec une formule décrivant le modèle.<br\>

**maxO3 ~ T9+ T12+T15+Ne9+Ne12+Ne15+Vx9+Vx12+Vx15+maxO3v'** <br\>


```R
# regression multiple
reg.mul <-
```


```R
# OU comme ozone2 ne contient que les variables explicatives et la variable a expliquer
reg.mul <- lm(maxO3 ~ ., data=ozone2)
ls()
```

#### Choix des variables

Il est possible avec l'analyse de **reg.mul** de faire un choix des variables à la main. On enlève la moins significative puis on recalcule les estimations et ainsi de suite.

Il existe aussi un package R qui traite du choix des variables: le package **leaps** et la fonction **regsubsets**


```R
library("leaps", lib.loc="~/R/lib")
library("lattice")
choix <- 
plot(choix)
```

On conserve les variables pour lesquelles le critère **"bib"** est minimum: T12, Ne9, Vx9 et max03v. D'autres critères de sélection sont disponibles.


```R
# regression après selection des variables
reg.fin <- 
summary(reg.fin)
```

#### Analyser les résidus: 

Même traitement qu'avec la régression simple.

#### Prévoir une nouvelle valeur: même traitement qu'avec la régression simple


```R
# Prediction + interval de confiance
xnew <- matrix(c(19,3,30,6.5),nrow=1)
colnames(xnew) <- c("T12","Ne9","Vx9","maxO3v")
xnew <- 
ynew <- 
class(ynew)
dimnames(ynew)
```

### Analyse de variance à un facteur

L'analyse de variance (ou ANOVA) à 1 facteur est une méthode statistique permettant de modéliser la relation entre **une variable explicative qualitative** A et **une variable à expliquer quantitative** Y. L'objectif principal étant de comparer les moyennes empiriques de Y pour les modalités de A.

On reprend notre étude, il s'agit d'analyser la relation entre le maximum journalier de la concentration d'ozone et la direction du vent classée en secteurs (Nord, Sud,Est,Ouest). La variable **vent** du fichier **ozone** à 4 modalités.

** Les diférentes étapes **

#### Importer les données et conserver les données utiles.


```R
# Lecture des données
names(ozone)
# Extraction des données utiles
ozone3 <- ...
names(ozone3)
summary(ozone3)
```

#### Représenter les données

Avant une analyse de variance, il est usuel de représenter les boîtes à moustaches de la variable à expliquer par modalité de la variable explicative.

La direction du vent a-t-elle un effet sur la dispersion du maximum de la concentration en ozone. L'analyse de variance permettra ensuite de vérifier si un effet observé sur les statistiques descriptives et/ou les graphiques est significatif ou pas.


```R
# boxplot par modalité
...
```

#### Analyse de la significativité du facteur

On utilise la fonction **lm** avec une formule pour estimer les paramètres du modèle. La fonction **anova** retourne le tableau d'analyse de variance.


```R
# Estimation des parametres
reg.aov1 <- 
```


```R
# Tableau d'analyse de variance

```

#### Analyser les résidus

Même principe que précédemment. Utiliser le package **lattice** pour représenter les résidus selon les modalités de la variable **vent**.



```R
res.aov1 <- rstudent(reg.aov1)
library(lattice)
monpanel <- function(...){
    panel.xyplot(...)
    panel.abline(h=c(-2,0,2),lty=c(3,2,3),...)
}
trellis.par.set(list(fontsize=list(point=5,text=8)))
xyplot(res.aov1~I(1:112)|vent,data=ozone3,pch="+",ylim=c(-3,3),
      panel=monpanel,ylab="Résidus",xlab="")
```

#### Interprétation des coefficients

Pour préciser comment la direction du vent influe sur le maximum d'ozone, on analyse les coefficients à l'aide du test de student.


```R
summary(reg.aov1)
```

### Analyse de variance avec interaction

C'est une méthode permettant de modéliser la relation entre **une variable quantitative** et **plusieurs variables qualitatives**.

On reprend notre étude, il s'agit d'analyser la relation entre *le maximum journalier de la concentration d'ozone* et **la direction du vent classée en secteurs** (Nord, Sud,Est,Ouest).  et **la précipitation** classée en deux modalités (Sec et Pluie).

**Les diférentes étapes**

#### Importer les données et conserver les données utiles.


```R
# Lecture des données
names(ozone)
# Extraction des données utiles
ozone4 <- ...
names(ozone4)
summary(ozone4)
```

#### Représentation des données

On représente une boîte à moustaches de la variable à expliquer par croisement des modalités des variables explicatives **vent** et **pluie** (4*2).

L'influence conjointe entre les variables **vent** et **pluie** a-t-elle un effet sur la dispersion du maximum de la concentration en ozone. 


```R
# Representation des donnees
...
# Interaction
par(mfrow=c(1,2))
with(ozone,interaction.plot(vent,pluie,maxO3))
with(ozone,interaction.plot(pluie,vent,maxO3))
```

#### Choisir le modèle - Estimation des paramètres



```R
# Choisir le modèle
mod.int <- 
anova(mod.int)
```


```R
# Choisir le modèle
mod.ssint <- 
anova(mod.ssint)
```

#### Interprétation des coefficients

Comme pour les méthodes précédentes, on utilisera la fonction **summary** pour aider l'interprétation.


```R
...
```

Une interprétation plus approfondie serait nécessaire mais ça n'est pas le propos de ce cours.

Pour finaliser cet exercice, il serait intéressant de faire un script R sous RStudio qui exécute globalement ou par méthode l'ensemble des instructions de cette étude. Pour l'exécuter par méthod, utiliser la possibilté de fournir un argument à un script.
