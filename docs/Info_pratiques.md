# Informations pratiques

## Intervenants

   Laurence Viry (AIRSEA/LJK - MaiMoSiNE) Laurence.Viry@univ-grenoble-alpes.fr

   Florian Privé () florian.prive@univ-grenoble-alpes.fr
    
## Lieu de la formation

   Salle de formation, RDC bâtiment IMAG (couloir à droite après les ascenseurs, face à l'accueil)
       Campus universitaire - 700 av. centrale 
   38400 Saint Martin D’Hères.

[Plan d'accès](https://batiment.imag.fr/fr/contact-adresses-plan-dacces)

## Planning

   [ADUM - catatlogue formation CED](https://www.adum.fr//script/formations.pl?mod=192255&menu_transparent=oui&site=CDUDG)
   
## Mise en oeuvre de la formation

* La formation se fera à l'aide de **notebooks R** en utilisant le serveur de notebooks et le serveurs Gitlab de GRICAD pour stocker/récupérer l'ensemble des informations nécessaires à la formation. Sur ce serveur seront installés R et les packages utiles à la formation.

Vous trouverez dans **Consignes.md**, les modalités d'accès à ces serveurs et quelques références à de la documentation.

* Il est conseillé aux participants d'apporter leur portable personnel sur lequel seront installés R et RStudio. Pour une aide à ces installations, voir ** install.md **.

* Des cas d'école seront fournis pour tous les exercices proposés, mais nous invitons également les participants à préparer un jeu de données personnelles pour les TPs. .

