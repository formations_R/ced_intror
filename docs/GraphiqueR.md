
Les graphiques constituent souvent le point de départ d'une analyse statistique. 

Pour visualiser les données avec R, on dispose d'au moins trois packages à savoir **graphics** pour réaliser les graphiques de base R, **lattice** qui permet, en autre, de représenter des données dans lesquelles la variable en ordonnée dépend de plusieurs variables et enfin **ggplot2** (où *gg* signifie "**G**rammar of **G**raphics") qui permet à travers une grammaire graphique, de concevoir des graphiques couche par couche et qui offre un haut niveau de contrôle sur les composantes graphiques.

On verra dans ce cours, l'étude des graphiques conventionnels (graphics), des références de tutoriaux vous seront fournies pour l'utilisation de **lattice** et **ggplot2**.

Pour commencer il peut être intéressant de regarder quelques exemples de représentations graphiques pouvant être construites avec R, vous utiliserez la fonction **demo** (script: ~/CED-IntroR/TP/exemples/demosGraphics.R):


```R
demo(graphics)  
```

## Les fonctions graphiques de base

Les fonctions couramment utilisées.

* Variables qualitatives <br />
**pie(x)** # diagramme camenbert <br />
**barplot(x)** # diagramme bâton 


* Variables quantitatives <br />
**hist(x,nclass)** # histogramme de x <br />
**boxplot(x)** # boite à moustache <br />
**stripchart**(x) <br />


* Graphiques 3D <br />
**image(x,y,z)** # forme d'image <br />
**persp(x,y,z)** # forme de nappe c <br />
**contour(x,y,z)** # les contours <br />
**Fonction utile** : z=outer(x,y,fonction)


* Tableau et matrices<br />
**pairs(data)** # nuage de points colonne par colonne de data<br />
**matplot(data)** # trace chaque colonne de data

### La fonction plot

La fonction **plot** est une fonction générique de R permettant de représenter tous les types de données.

#### Nuage de points d'une variable y en fonction d'une variable x

Représenter à intervalles réguliers les points de la fonction: $\bf{ x \longrightarrow sin(2\pi x) \quad x \in [0,1[}$


```R
# Construire la grille de points
grillex <- seq(0,1,length=50)
fx <- sin(2*pi*grillex)
plot(x=grillex,y=fx)
```


![png](GraphiqueR_files/GraphiqueR_3_0.png)


Il est plus classique d'utiliser la fonction plot avec des formules du type  $\bf{y^{\sim}x}$


```R
plot(fx~grillex)
```

#### A partir d'un data-frame

On reprend le fichier de données **mtcars**

   *Description* : les données ont été extraites du magazine Motor Trend US de 1974 et comprennent la consommation de carburant et 10 aspects de la conception et des performances de 32 automobiles (modèles 1973-1974).

   *Format*:

Un fichier de données "mtcars" avec 32 observations sur 11 variables:

mpg: Miles/(US) gallon (variable quantitative)
cyl: Number of cylinders (variable qualitative)
disp: Displacement (cu.in.) (variable quantitative)
hp:    Gross horsepower (variable quantitative)
drat: Rear axle ratio (variable quantitative)
wt:    Weight (1000 lbs) (variable quantitative)
qsec: 1/4 mile time (variable quantitative)
vs: V/S (variable qualitative)
am:    Transmission (0 = automatic, 1 = manual) (variable qualitative)
gear: Number of forward gears (variable qualitative)
carb: Number of carburetors (variable qualitative)



```R
mtcars <- read.csv("data/mtcars.csv",header=TRUE,sep=",")
str(mtcars)
summary(mtcars)
# Definir en tant que variable qualitative
mtcars$cyl <- factor(mtcars$cyl)
mtcars$vs <- factor(mtcars$vs)
mtcars$am <- factor(mtcars$am)
mtcars$gear <- factor(mtcars$gear)
mtcars$carb <- factor(mtcars$carb)
summary(mtcars)
```

    'data.frame':	32 obs. of  11 variables:
     $ mpg : num  21 21 22.8 21.4 18.7 18.1 14.3 24.4 22.8 19.2 ...
     $ cyl : int  6 6 4 6 8 6 8 4 4 6 ...
     $ disp: num  160 160 108 258 360 ...
     $ hp  : int  110 110 93 110 175 105 245 62 95 123 ...
     $ drat: num  3.9 3.9 3.85 3.08 3.15 2.76 3.21 3.69 3.92 3.92 ...
     $ wt  : num  2.62 2.88 2.32 3.21 3.44 ...
     $ qsec: num  16.5 17 18.6 19.4 17 ...
     $ vs  : int  0 0 1 1 0 1 0 1 1 1 ...
     $ am  : int  1 1 1 0 0 0 0 0 0 0 ...
     $ gear: int  4 4 4 3 3 3 3 4 4 4 ...
     $ carb: int  4 4 1 1 2 1 4 2 2 4 ...



          mpg             cyl             disp             hp       
     Min.   :10.40   Min.   :4.000   Min.   : 71.1   Min.   : 52.0  
     1st Qu.:15.43   1st Qu.:4.000   1st Qu.:120.8   1st Qu.: 96.5  
     Median :19.20   Median :6.000   Median :196.3   Median :123.0  
     Mean   :20.09   Mean   :6.188   Mean   :230.7   Mean   :146.7  
     3rd Qu.:22.80   3rd Qu.:8.000   3rd Qu.:326.0   3rd Qu.:180.0  
     Max.   :33.90   Max.   :8.000   Max.   :472.0   Max.   :335.0  
          drat             wt             qsec             vs        
     Min.   :2.760   Min.   :1.513   Min.   :14.50   Min.   :0.0000  
     1st Qu.:3.080   1st Qu.:2.581   1st Qu.:16.89   1st Qu.:0.0000  
     Median :3.695   Median :3.325   Median :17.71   Median :0.0000  
     Mean   :3.597   Mean   :3.217   Mean   :17.85   Mean   :0.4375  
     3rd Qu.:3.920   3rd Qu.:3.610   3rd Qu.:18.90   3rd Qu.:1.0000  
     Max.   :4.930   Max.   :5.424   Max.   :22.90   Max.   :1.0000  
           am              gear            carb      
     Min.   :0.0000   Min.   :3.000   Min.   :1.000  
     1st Qu.:0.0000   1st Qu.:3.000   1st Qu.:2.000  
     Median :0.0000   Median :4.000   Median :2.000  
     Mean   :0.4062   Mean   :3.688   Mean   :2.812  
     3rd Qu.:1.0000   3rd Qu.:4.000   3rd Qu.:4.000  
     Max.   :1.0000   Max.   :5.000   Max.   :8.000  



          mpg        cyl         disp             hp             drat      
     Min.   :10.40   4:11   Min.   : 71.1   Min.   : 52.0   Min.   :2.760  
     1st Qu.:15.43   6: 7   1st Qu.:120.8   1st Qu.: 96.5   1st Qu.:3.080  
     Median :19.20   8:14   Median :196.3   Median :123.0   Median :3.695  
     Mean   :20.09          Mean   :230.7   Mean   :146.7   Mean   :3.597  
     3rd Qu.:22.80          3rd Qu.:326.0   3rd Qu.:180.0   3rd Qu.:3.920  
     Max.   :33.90          Max.   :472.0   Max.   :335.0   Max.   :4.930  
           wt             qsec       vs     am     gear   carb  
     Min.   :1.513   Min.   :14.50   0:18   0:19   3:15   1: 7  
     1st Qu.:2.581   1st Qu.:16.89   1:14   1:13   4:12   2:10  
     Median :3.325   Median :17.71                 5: 5   3: 3  
     Mean   :3.217   Mean   :17.85                        4:10  
     3rd Qu.:3.610   3rd Qu.:18.90                        6: 1  
     Max.   :5.424   Max.   :22.90                        8: 1  


* Représentation de deux variables qualitatives: les variables **drat** et **wt**


```R
plot(mtcars[,"drat"],mtcars[,"wt"])
```


![png](GraphiqueR_files/GraphiqueR_9_0.png)


Comme les deux variables sont contenues dans le même *data-frame*, une syntaxe plus simple permet d'avoir directement les noms de variables en labellés d'axe.


```R
plot(drat~wt,data=mtcars)
```


![png](GraphiqueR_files/GraphiqueR_11_0.png)


Cette représenation graphique aurait pu être obtenue en définissant de manière explicite les labels des axes à l'appel de la fonction "plot" avec les arguments **xlab** et **ylab**.


```R
plot(mtcars[,"drat"],mtcars[,"wt"],xlab="wt",ylab="drat")
```

* Pour représenter une variable quantitative ("wt") en fonction d'une variable qualitative ("cyl"), on utilisera la même syntaxe:


```R
plot(wt~cyl,data=mtcars,xlab="Number of cylinders",ylab="Weight (1000 lbs)")
```


![png](GraphiqueR_files/GraphiqueR_15_0.png)


La fonction plot retourne une boîte à moustaches par modalité de la variable qualitative. Ce graphique permet de voir rapidement si il existe un effet de la variable *Numbers of cylinders* sur la varibale *Weight*. Il peut être obtenu à partir de la fonction **boxplot**.


```R
boxplot(wt~cyl,data=mtcars)
```


    Error in eval(expr, envir, enclos): object 'wt' not found
    Traceback:


    1. boxplot(wt ~ cyl)

    2. boxplot.formula(wt ~ cyl)

    3. eval(m, parent.frame())

    4. eval(expr, envir, enclos)

    5. stats::model.frame(formula = wt ~ cyl)

    6. model.frame.default(formula = wt ~ cyl)

    7. eval(predvars, data, env)

    8. eval(expr, envir, enclos)


* Représenation de deux variables qualitatives par un diagramme bande:


```R
plot(cyl~vs,data=mtcars)
```

Pour chaque modalité du facteur explicatif ("vs") on la fréquence relative de chaque modalité du facteur à expliquer ("cyl") et la largeur de bande est proportionnelle à la fréquence de la modalité du facteur explicatif("vs").

* On peut **représenter une variable qualitative ("vs") en fonction d'une variable quantitative ("drat")**. La variable quantitative est découpée en classes selon la même méthode qu'un histogramme et dans chaque classe sont calculées les fréquences relatives de chaque modalité de la variable qualitative.


```R
plot(cyl~drat,data=mtcars)
```


![png](GraphiqueR_files/GraphiqueR_21_0.png)


Ce graphique peut être obtenu directement à partir de la fonction **splineplot**.


```R
spineplot(cyl~drat,data=mtcars)
```


![png](GraphiqueR_files/GraphiqueR_23_0.png)



```R
plot(mtcars[,"cyl"]~mtcars[,"drat"],xlab="Rear axle ratio",ylab="Number of cylinders")
```


![png](GraphiqueR_files/GraphiqueR_24_0.png)



```R
colorInd <-function(v,seuil)
{
  sapply(v,function(x) {if (x < seuil ) tabcol="black" else tabcol="red"})
}
 colS <- colorInd(mtcars$drat,3.5)
plot(mtcars[,"drat"], ylab="Rear axle ratio",cex=0.5, pch=16,col=colS)
```


![png](GraphiqueR_files/GraphiqueR_25_0.png)



```R
plot(mtcars[,"drat"], ylab="Rear axle ratio",cex=0.5, pch=16,col="red")
```

## Ajout à un graphique

Une fois le graphique tracé, on peut le compléter par d'autres informations:

* ajout de *lignes* avec la fonction **lines()**
* ajout de *text* avec la fonction **text()**


```R
x<- 1:10;y <-x^2;z <-x^3;w<-x^(-2)
plot(x,y,type="l",main="Fonctions puissances",cex.main=0.9)
text(4,y[4]+5,expression(x^2))

lines(x,z,col="red")
text(2,z[2]+5,expression(x^3))
lines(x,w,col="blue")
text(6,w[6]+5,expression(x^-2))
```


![png](GraphiqueR_files/GraphiqueR_28_0.png)


* ajout de *points* par la fonction **point()**
* ajout de *flèches* par la fonction **arrow()**
* ajout de *segments* par la fonction **segments()**
* ajout de *polygones* par la fonction **polygone()**

## Plusieurs graphiques dans la même fenêtre

On veut faire figurer plusieurs graphiques dans une même fenêtre.

* Pour des graphiques de même taille: on utilise la **fonction par()**.<bf\>
 
 L'instruction **par(mfrow=c(n,p))** organise **n\*p** graphiques en *n lignes et p
colonnes*.


```R
# Les donnees
rm(list=ls())
data(iris)
```


```R
par(mfrow=c(2,2)) # une ligne et deux colonnes
plot(Sepal.Length~Sepal.Width,data=iris,pch=0)
plot(Petal.Length~Petal.Width,data=iris,pch=16)
plot(Sepal.Length~Sepal.Width,data=iris,pch=0)
plot(Petal.Length~Petal.Width,data=iris,pch=16)
```


![png](GraphiqueR_files/GraphiqueR_33_0.png)


* Pour des graphiques de taille différentes, on utilisera la fonction **layout**.
Elle admet comme argument, une matrice de taille **nrow X ncol**, les valeurs de la
matrice correspondent aux numéros des graphiques qui doivent être dessinés dans
chaque case.
Pour disposer trois graphiques sur deux lignes:

$$mat =\left[\begin{array}{cc}
1&1 \\ 2&3
\end{array}\right]$$

Trois graphiques sur deux lignes:


```R
mat <-matrix(c(1,2,1,3),nrow=2)
layout(mat)
plot(Sepal.Length~Species,data=iris,pch=0)
plot(Sepal.Length~Sepal.Width,data=iris,pch=21)
plot(Petal.Length~Petal.Width,data=iris,pch=16)
```


![png](GraphiqueR_files/GraphiqueR_35_0.png)


## Personnalisation des graphiques

Certains paramètres sont modifiables directement dans la commande graphique, d'autres sont accessibles dans la fonction par qui gère tous les paramètres graphiques du device:

* Utiliser des couleurs avec **col="red"**, en utilisant des chiffres ou un code
**"RGB"**.

* de mettre un titre avec l'argument **"main"**
* Contrôler l'aspect des axes et de leur label.

Certains paramètres sont modifiables directement dans l'appel de la fonction graphique, d'autres sont accessibles par la fonction **par()**.


```R
x <- 1:10
y <- x^2
par(fg='blue',bg='#f2a1c2') # couleur en avant plan et en arrière plan
plot(x,y,type="l",axes=FALSE,xlab="x",ylab="y", main=expression(f(x) == x^2),cex.main=0.9)
axis(1,at=c(1,5,10),label=c("coord 1","coord 2","coord 3"), cex.axis=0.8)
axis(2,at=seq(1,100,length=10),cex.axis=0.8)
```


```R
help(par)
```

* On peut ajouter une légende:

On ajoute une légende avec la fonction legend


```R
ecarty <- range(iris[,"Sepal.Length"])
plot(iris[1:75,"Sepal.Length"],type="l")
lines(iris[76:150,"Sepal.Length"],ylim=ecarty,col="red")
legend("topleft",legend=c("sem1","sem2"),col=c("black","red"),lty=1)
```


![png](GraphiqueR_files/GraphiqueR_40_0.png)


* On peut insérer des symboles ou des formules mathématiques en utilisant la syntaxe "latex":


```R
plot(1,1,cex.lab=0.8,xlab=expression(bar(x) == sum(frac(x[i],10),i==1,10)))
```


![png](GraphiqueR_files/GraphiqueR_42_0.png)



```R
x <- seq(-4, 4, len = 101)
y <- cbind(sin(x), cos(x))
matplot(x, y, type = "l", xaxt = "n",main = expression(paste(plain(sin) * phi, " and ",
 plain(cos) * phi)),
 ylab = expression("sin" * phi, "cos" * phi), # only 1st is + xlab = expression(paste("Phase Angle ", phi)),
 col.main = "blue")
axis(1, at = c(-pi, -pi/2, 0, pi/2, pi),labels = expression(-pi, -pi/2, 0, pi/2, pi))
```


![png](GraphiqueR_files/GraphiqueR_43_0.png)


Pour connaître l'ensemble des paramètres qui permettent d'améliorer les
graphiques, voir la fonction **par()** (help(par))

## Représentation de distributions

Pour représenter la distribution d'une variable continue, il existe des solutions classiques déjà programmées.

### Histogramme


```R
par(mfrow=c(1,3))
hist(iris$Sepal.Length,col="skyblue",cex.main=0.8)
hist(iris$Sepal.Length,col="red",main="Histogramme de Sepal.Length",prob=TRUE,xlab="Sepal.Length",cex.main=0.8)
plot(density(iris$Sepal.Length),cex.main=0.8,main="Noyaux",xlab="Sepal.Length")
```


![png](GraphiqueR_files/GraphiqueR_46_0.png)


L'histogramme est un estimateur de la densité si on indique **prob=TRUE**

### Barplot


```R
data(iris)
```


```R
Sepal.Length <- cut(iris$Sepal.Length,4)
Sepal.Length
```


<ol class=list-inline>
	<li>(4.3,5.2]</li>
	<li>(4.3,5.2]</li>
	<li>(4.3,5.2]</li>
	<li>(4.3,5.2]</li>
	<li>(4.3,5.2]</li>
	<li>(5.2,6.1]</li>
	<li>(4.3,5.2]</li>
	<li>(4.3,5.2]</li>
	<li>(4.3,5.2]</li>
	<li>(4.3,5.2]</li>
	<li>(5.2,6.1]</li>
	<li>(4.3,5.2]</li>
	<li>(4.3,5.2]</li>
	<li>(4.3,5.2]</li>
	<li>(5.2,6.1]</li>
	<li>(5.2,6.1]</li>
	<li>(5.2,6.1]</li>
	<li>(4.3,5.2]</li>
	<li>(5.2,6.1]</li>
	<li>(4.3,5.2]</li>
	<li>(5.2,6.1]</li>
	<li>(4.3,5.2]</li>
	<li>(4.3,5.2]</li>
	<li>(4.3,5.2]</li>
	<li>(4.3,5.2]</li>
	<li>(4.3,5.2]</li>
	<li>(4.3,5.2]</li>
	<li>(4.3,5.2]</li>
	<li>(4.3,5.2]</li>
	<li>(4.3,5.2]</li>
	<li>(4.3,5.2]</li>
	<li>(5.2,6.1]</li>
	<li>(4.3,5.2]</li>
	<li>(5.2,6.1]</li>
	<li>(4.3,5.2]</li>
	<li>(4.3,5.2]</li>
	<li>(5.2,6.1]</li>
	<li>(4.3,5.2]</li>
	<li>(4.3,5.2]</li>
	<li>(4.3,5.2]</li>
	<li>(4.3,5.2]</li>
	<li>(4.3,5.2]</li>
	<li>(4.3,5.2]</li>
	<li>(4.3,5.2]</li>
	<li>(4.3,5.2]</li>
	<li>(4.3,5.2]</li>
	<li>(4.3,5.2]</li>
	<li>(4.3,5.2]</li>
	<li>(5.2,6.1]</li>
	<li>(4.3,5.2]</li>
	<li>(6.1,7]</li>
	<li>(6.1,7]</li>
	<li>(6.1,7]</li>
	<li>(5.2,6.1]</li>
	<li>(6.1,7]</li>
	<li>(5.2,6.1]</li>
	<li>(6.1,7]</li>
	<li>(4.3,5.2]</li>
	<li>(6.1,7]</li>
	<li>(4.3,5.2]</li>
	<li>(4.3,5.2]</li>
	<li>(5.2,6.1]</li>
	<li>(5.2,6.1]</li>
	<li>(5.2,6.1]</li>
	<li>(5.2,6.1]</li>
	<li>(6.1,7]</li>
	<li>(5.2,6.1]</li>
	<li>(5.2,6.1]</li>
	<li>(6.1,7]</li>
	<li>(5.2,6.1]</li>
	<li>(5.2,6.1]</li>
	<li>(5.2,6.1]</li>
	<li>(6.1,7]</li>
	<li>(5.2,6.1]</li>
	<li>(6.1,7]</li>
	<li>(6.1,7]</li>
	<li>(6.1,7]</li>
	<li>(6.1,7]</li>
	<li>(5.2,6.1]</li>
	<li>(5.2,6.1]</li>
	<li>(5.2,6.1]</li>
	<li>(5.2,6.1]</li>
	<li>(5.2,6.1]</li>
	<li>(5.2,6.1]</li>
	<li>(5.2,6.1]</li>
	<li>(5.2,6.1]</li>
	<li>(6.1,7]</li>
	<li>(6.1,7]</li>
	<li>(5.2,6.1]</li>
	<li>(5.2,6.1]</li>
	<li>(5.2,6.1]</li>
	<li>(5.2,6.1]</li>
	<li>(5.2,6.1]</li>
	<li>(4.3,5.2]</li>
	<li>(5.2,6.1]</li>
	<li>(5.2,6.1]</li>
	<li>(5.2,6.1]</li>
	<li>(6.1,7]</li>
	<li>(4.3,5.2]</li>
	<li>(5.2,6.1]</li>
	<li>(6.1,7]</li>
	<li>(5.2,6.1]</li>
	<li>(7,7.9]</li>
	<li>(6.1,7]</li>
	<li>(6.1,7]</li>
	<li>(7,7.9]</li>
	<li>(4.3,5.2]</li>
	<li>(7,7.9]</li>
	<li>(6.1,7]</li>
	<li>(7,7.9]</li>
	<li>(6.1,7]</li>
	<li>(6.1,7]</li>
	<li>(6.1,7]</li>
	<li>(5.2,6.1]</li>
	<li>(5.2,6.1]</li>
	<li>(6.1,7]</li>
	<li>(6.1,7]</li>
	<li>(7,7.9]</li>
	<li>(7,7.9]</li>
	<li>(5.2,6.1]</li>
	<li>(6.1,7]</li>
	<li>(5.2,6.1]</li>
	<li>(7,7.9]</li>
	<li>(6.1,7]</li>
	<li>(6.1,7]</li>
	<li>(7,7.9]</li>
	<li>(6.1,7]</li>
	<li>(5.2,6.1]</li>
	<li>(6.1,7]</li>
	<li>(7,7.9]</li>
	<li>(7,7.9]</li>
	<li>(7,7.9]</li>
	<li>(6.1,7]</li>
	<li>(6.1,7]</li>
	<li>(5.2,6.1]</li>
	<li>(7,7.9]</li>
	<li>(6.1,7]</li>
	<li>(6.1,7]</li>
	<li>(5.2,6.1]</li>
	<li>(6.1,7]</li>
	<li>(6.1,7]</li>
	<li>(6.1,7]</li>
	<li>(5.2,6.1]</li>
	<li>(6.1,7]</li>
	<li>(6.1,7]</li>
	<li>(6.1,7]</li>
	<li>(6.1,7]</li>
	<li>(6.1,7]</li>
	<li>(6.1,7]</li>
	<li>(5.2,6.1]</li>
</ol>




```R
# Sepal.Length est de class factor
par(mfrow=c(1,2))
summary(Sepal.Length)
#
barplot(table(Sepal.Length),col=1:nlevels(Sepal.Length),legend=TRUE)
barplot(table(Sepal.Length),col=1:nlevels(Sepal.Length),horiz=TRUE)
```


<dl class=dl-horizontal>
	<dt>(4.3,5.2]</dt>
		<dd>45</dd>
	<dt>(5.2,6.1]</dt>
		<dd>50</dd>
	<dt>(6.1,7]</dt>
		<dd>43</dd>
	<dt>(7,7.9]</dt>
		<dd>12</dd>
</dl>




![png](GraphiqueR_files/GraphiqueR_51_1.png)


### pie: diagramme circulaire (camembert)


```R
pie(table(Sepal.Length),col=1:nlevels(Sepal.Length),radius = 1.)
```


![png](GraphiqueR_files/GraphiqueR_53_0.png)


### Diagramme en barre par groupe



```R
# On decoupe la variable iris$Sepal.Width en 5 classes
class(iris$Sepal.Width)
Sepal.Width <- cut(iris$Sepal.Width,5)
class(Sepal.Width)
```


'numeric'



'factor'



```R
par(mfrow=c(1,3))
barplot(table(Sepal.Length,Sepal.Width),col=1:nlevels(Sepal.Length),legend=TRUE)
#
barplot(table(Sepal.Length,Sepal.Width),col=1:nlevels(Sepal.Length),beside=TRUE)
#
barplot(table(Sepal.Length,Sepal.Width),col=1:nlevels(Sepal.Length),beside=TRUE,horiz=TRUE)
```


![png](GraphiqueR_files/GraphiqueR_56_0.png)


## Graphiques en plusieurs dimensions

Les fonctions de représentation 3D sur une grille de points sont les fonctions
**persp**(3D avec effet de perspective), **contour** (lignes de niveau) et **image** (lignes
de niveau avec effet de couleur).


```R
f <- function(x,y) 10*sin(sqrt(x^2+y^2))/sqrt(x^2+y^2)
x <-seq(-10,10,length=30) # maillage
y <- x
```

* Evaluons la fonction en chaque point de la grille avec la fonction **outer()**.


```R
z <-outer(x,y,f)
```

* Traçons la fonction 3D avec la fonction **persp()**.


```R
persp(x,y,z,theta=30,phi=30,expand=0.5)
```


![png](GraphiqueR_files/GraphiqueR_62_0.png)


Nous pouvons obtenir les **courbes de niveaux** en utilisant la fonction **contour** (lignes de
niveau) ou la fonction **image()** (lignes de niveau avec effet de couleur).


```R
par(mfrow=c(1,2))
contour(x,y,z)
image(x,y,z)
```


![png](GraphiqueR_files/GraphiqueR_64_0.png)


Nous pouvons utiliser le package **rgl** pour construire la surface de réponse du
graphique précédent.


```R
library(rgl)
rgl.surface(x,y,z)
plot3d(x,y,z)
```


    Error in library(rgl): there is no package called ‘rgl’
    Traceback:


    1. library(rgl)

    2. stop(txt, domain = NA)


## Package lattice

### Livres:

* Lattice  Multivariate Data Visualization with R - Deepayan Sarkar (Edition Springer)
    (web: http://lmdvr.r-forge.r-project.org/figures/figures.html)

### Web

* [Introduction to R - Lattice](https://www.isid.ac.in/~deepayan/R-tutorials/labs/04_lattice_lab.pdf)


## Package ggplot2

### Web
* [Introduction to R graphics with ggplot2 - R Tutorials](http://tutorials.iq.harvard.edu/R/Rgraphics/Rgraphics.html)
* [Data Visualization with ggplot2](https://www.datacamp.com/courses/data-visualization-with-ggplot2-1)
* https://fr.slideshare.net/VincentIsoz/tutorial-ggplot2

### Livres

* Elegant Graphics for Data Analysis - Hadley Wickham (Edition Springer)

* R Graphics Cookbook - Winston Chang (Edition O'Reilly)
