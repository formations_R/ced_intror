
# Introduction
## Qu'est ce que R ?
R est un système d'analyse statistique et graphique créé par Ross Ihaka et Robert Gentleman.
R est à la fois un logiciel et un langage interprété dont la conception dérive du langage S développé au sein des laboratoires AT&T Bell dans les années 80 et disponible actuellement sous la forme du logiciel S-PLUS commercialisé par la compagnie Insightful. Il y a des différences dans la conception de R et celle de S, pour en savoir plus sur ce point, se reporter au [R-FAQ3](https://cran.r-project.org/doc/FAQ/R-FAQ.html).

Le langage R possède aujourd'hui une communauté mature d'utilisateurs et de développeurs qui ont créé et partagé des milliers de package via le _Comprehensive R Archive Network_ ([CRAN](https://cran.r-project.org)).

R comporte de nombreuses fonctions pour les analyses statistiques, les graphiques, le traitement et le stockage des données. Les graphiques sont visualisés à l'écran ou sur papier et peuvent être exportés sous divers formats (jpg, png, bmp, ps, pdf, emf, pictex, svg, xfig); les formats disponibles peuvent dépendre du système d'exploitation.
Les résultats des analyses statistiques sont affichés à l'écran ou peuvent être sauvegardés ou exportés dans un fichier pour être utilisés dans des analyses ultérieures.

R est un langage de programmation simple et efficace qui permet à l'utilisateur d'écrire ses propres algorithmes en utilisant le traitement conditionnel, les boucles, la récursivité, les fonctions intrinsèques et utilisateurs et des fonctions d'entrée/sortie. 

R comprend une suite d'opérateurs pour les calculs sur des tableaux, en particulier des matrices.

R offre les possibilités d'améliorer ses performances en convertisant le haut niveau d'interprétation du code R en un langage compilé écrit en C, C++ ou Fortran (**Rcpp**, ...).
R permet de s'adapter à l'architecture des processeurs multi-coeurs et aux clusters de plusieurs noeuds en fournissant des outils aux développeurs qui leur permettent d'utiliser des techniques de programmation parallèle (**parallel**, **multicore**, **snow**, ...).

Un dernier point important: les statisticiens ont implémentés dans R des centaines de procédures adaptées à une grande variété d'applications, directement intégrées dans R sous forme de packages.

## Pourquoi R ?
### Avantages

R a des avantages majeurs:

1. Il est gratuit et le restera puisqu'il est distribué librement sous les termes de la [GNU General Public Licence](http://www.gnu.org/).
2. Il est disponible sur Internet, via un grand réseau de serveurs.
3. Il fonctionne sur de nombreux systèmes d'exploitation Unix et dérivés incluant Darwin, Mac OS X, Linux, FreeBSD et Solaris; sur Microsoft Windows.
4. Il est le produit d'une collaboration internationale entre statisticiens et informaticiens.
5. Il ne limite pas l'utilisateur à un ensemble de procédures ou d'options ou à une méthode presse-bouton, tout en utilisant les packages déjà intégrés dans R.
6. Il fournit des outils consistants (bibliothèques, language, ...) qui permettent de travailler avec des objets complexes de grande taille. 
7. Il existe une documentation technique complète à laquelle participent les utilisateurs, des tutoriaux adaptés à des usages différents et aussi plusieurs bons livres sur les méthodes statistiques qui utilisent R (ou S) pour l'illustration.
8. Il est entièrement programmable, les procédures répétitives peuvent facilement être automatisées par des scripts écrits par l'utilisateur. Il est facile d'écrire ses propres fonctions, et pas trop compliqué d'écrire des paquets entiers pour implémenter de nouveaux algorithmes.
9. Le code source est publié, ainsi vous avez accès aux algorithmes exacts avec une validation possible par des statisticiens experts.
10. Il peut échanger des données en format MS-Excel, texte, format fixe et délimité (CSV, ...), de sorte que les jeux de données existants sont facilement importés, et les résultats calculés en R facilement exportables. 
11. La plupart des programmes écrits avec S-PLUS fonctionneront sans modification, ou avec des changements mineurs, dans R.

### Inconvénients

R présentent quelques inconvénients:

1. L'utilisateur doit définir lui-même la séquence des analyses et les exécuter pas à pas, cependant, il est facile de créer des scripts avec toutes les étapes dans les éditeurs des interfaces utilisateurs de R (dans ce cours, RStudio).
2. L'utilisateur doit apprendre à penser autrement la gestion de ses données, de penser les objets R comme des classes, ce qui lui permettra de bénéficier des avantages des langages orientés objet (les méthodes opérant différemment sur un objet en fonction de sa classe).
3. L'utilisateur doit apprendre un langage, à la fois pour les commandes mais aussi pour la spécification des analyses statistiques.

### Alternatives

Il y a d'autres logiciels qui permettent de faire des calculs statistiques et du traitement de données. Ces logiciels peuvent souvent interagir entre eux avec la possibilité de programmer ses propres algorithmes ou d'interagir avec R. Aucun d'entre eux n'est open-source, ce qui signifie que vous devez faire confiance à l'entreprise pour faire les calculs correctement

   - S-PLUS: : implémentation commerciale du langage S difusée par TIBCO.
   - SAS: un concurrent de S-PLUS, très utilisée dans l'industrie, programmable.
   - [Des programmes statistiques spécifques](http://www.stata.com/links/statistical-software-providers/).
   - Tableurs comme  Microsoft Excel.

## Installer R

R est distribué librement sous les termes de la [GNU General Public Licence](http://www.gnu.org/), son développement et sa distribution sont assurés par plusieurs personnes regroupées dans la [R core team](https://www.r-project.org/contributors.html).

Pour un meilleur suivi du cours, nous vous conseillons d'installer **R** (version 3.4 minimum) sur votre ordinateur personnel, il peut être téléchargé sur différents serveurs appelés “miroirs” et situés dans de nombreux pays. Plusieurs miroirs sont disponibles en France, on y trouvera des versions adaptées à différentes plateformes (Windows, Mac-OS et Linux).

Sous plusieurs formes, soit des exécutables précompilés pour Windows, Linux et OSX (Macintosh), soit des sources (écrits principalement en C et certaines routines en Fortran) qu'il faudra compiler avant de les utiliser.

   - [Installer R sous **Windows**](https://cloud.r-project.org/bin/windows/base/)

   - [Installer R sous **Mac**](https://cloud.r-project.org/bin/macosx/)
   
   - [Sous **Linux**](https://cran.r-project.org/mirrors.html), R n’est fourni que comme un outil en ligne de commande, utilisez votre gestionnaire de packages habituel (de préférence, choisir un miroir proche de chez vous).

Plusieurs packages sont fournis avec l'installation de base, de nouveaux packages peuvent être facilement ajoutés, l'installation peut se faire en local ou pour l'ensemble des utilisateurs. Nous verrons comment installer de nouveaux packages dans la suite du cours.

[RStudio](https://www.rstudio.com/products/RStudio/) est un environnement de développement intégré (IDE) qui propose des outils et facilite l’écriture de scripts et l’usage de R au quotidien. C’est une interface bien supérieure à celles fournies par défaut lorsqu’on installe R sous Windows ou sous Mac.

[Installer RStudio](https://www.rstudio.com/products/rstudio/download/#download), téléchargez la version adaptée à votre système.


## Utiliser R

Il y a plusieurs méthodes pour travailler avec R

* En **mode commande** (CLI) avec un éditeur et une interface graphique ou pas.
* À partir d'un environnement de développement intégré (IDE). L'objectif d'un IDE est d'automatiser et de simplifier les activités du programmeur. Suivant les systèmes d'exploitation, il existe des interfaces de développement différentes.
* En utilisant le module **ESS** (Emacs Speaks Statistics) de l'éditeur **emacs**.

Dans ce cours nous utiliserons l'IDE **RStudio** utilisable sur la plupart des plateformes (Windows, Linux, Mac).
 

## Utiliser R en mode Console

Le langage R est un *langage interprété*. Au démarrage d'une nouvelle session de R, on interagit avec R dans une **console**.

On écrit une ligne de code dans la **console**, on la valide (Entrée) et on observe le résultat (pas besoin d'une étape préalable de compilation du code).

### Pour lancer une console R

   - sous Windows, lancer le programme **Rxxx** (xxx correspondant au numéro de  version) dont un raccourci a été créé sur le bureau après l'installation.
 <img src="../figures/Console-R-Windows.jpg",width="60%",height="60%">  
   
   
   - sous Mac OS X, lancer le programme R présent dans le dossier Applications ou la commande **R** dans un terminal suivant le type d'installation de R choisi.
   <img src="../figures/Console-R-Macos.jpg",width="60%",height="60%">
   
   - sous Linux (et plus généralement tout système Unix), **ouvrir un terminal** et **lancer la commande R**.
    <img src="../figures/consoleLinux.jpg",width="60%",height="60%">  
    
   - Au premier lancement de RStudio, l’écran principal est découpé en trois grandes zones :

    <img src="../figures/interfaceRStudio_0.jpg",width="60%",height="60%">
    
**La zone de gauche est la console**. 
    
Les interfaces graphiques sont différentes entre les différents systèmes d'exploitation mais cela ne change rien au langage qui sera compatible avec toutes les OS.

Dans la suite du cours, nous utiliserons la console de **RStudio** ou les notebooks sur le serveur de notebooks de GRICAD.


## RStudio : interface conviviale

[RStudio](https://www.rstudio.com/products/RStudio/) est un environnement de développement intégré (IDE), qui 
propose des outils et facilite l’écriture de scripts et l’usage de R au quotidien. C’est une interface bien 
supérieure à celles fournies par défaut lorsqu’on installe R sous Windows ou sous Mac.

Des versions libres sont téléchargeables sur la page https://www.rstudio.com/products/rstudio/download/#download.

- Téléchargez la version adaptée à votre système et installez la sur votre poste de travail.

- Cet environnement inclut dans une seule interface attractive:  

      <img src="../figures/InterfaceRStudio.jpg",width="80%",height="80%">
      
     - une console (Console), 
     - un terminal (Terminal),
     - un éditeur de code, 
     - sorties graphiques (Plot), 
     - l'historique (History),
     - une aide en ligne (Help), 
     - le contenu de l'espace de travail (Environment),
     - un gestionnaire de fichier (Files),
     - le gestionnaire de paquets (Packages).
 
**Cette interface peut être personnalisée**, voir: [Customizing RStudio](https://support.rstudio.com/hc/en-us/articles/200549016-Customizing-RStudio).
 
On peut utiliser les gestionnaires de version comme **git** ou **svn** sous RStudio, ce qu'on ne fera pas dans ce cours. Cela fait partie [du coup de R avancé](https://privefl.github.io/advr38book/good-practices.html#git).
 

### Modification et exécution du code

L'éditeur de source de RStudio inclut une variété de fonctionnalités améliorant la productivité, y compris la mise en évidence de la syntaxe, l'achèvement du code, l'édition de plusieurs fichiers et la recherche/remplacement.

RStudio vous permet également d'exécuter de manière flexible le code R directement depuis l'éditeur de source.  Travailler dans l'éditeur de source facilite la reproduction de séquences de commandes et le regroupement de commandes pour une réutilisation en tant que fonction. 

Pour en savoir plus: [Editing and Executing Code](https://support.rstudio.com/hc/en-us/articles/200484448).

### Gestion de fichiers

On utilisera l'onglet **Files** de RStudio. Celui-ci permet également de créer de nouveaux répertoires (New Folder) et de définir le répertoire courant comme répertoire de travail (`Files -> More -> Set As Working Directory`).

### Historique

RStudio maintient un historique de toutes les commandes que vous avez déjà entrées dans la console. Vous pouvez parcourir et rechercher dans cet historique à l'aide de l'onglet **History**. 

Les commandes sélectionnées dans l'onglet **History** peuvent être utilisées de deux façons, correspondant aux deux boutons sur le côté gauche de la barre d'outils "Historique": 

- envoyer la(les) commande(s) sélectionnée(s) à la console, 
- insèrer la(les) commande(s) sélectionnée(s) dans le document source actif

Pour en savoir plus: [Using Command History](https://support.rstudio.com/hc/en-us/articles/200526217-Command-History).


Pour en savoir plus sur RStudio globalement: [Using RStudio IDE](https://support.rstudio.com/hc/en-us/sections/200107586-Using-the-RStudio-IDE)

## Utilisation de R comme une simple calculatrice 

R utilise des fonctions et des opérateurs qui agissent sur des objets:

- Addition       : **+** 
- Soustraction   : **-**
- Multiplication: **\***
- Division: **/**
- Exponentiation: **^**  élève le nombre à sa gauche à la puissance du nombre à sa droite, par exemple 3^2 = 9 .
- Modulo: **%%** reste de la division entière du nombre à sa gauche par le nombre à sa droite, 13%%3 = 1
- Division entière : **%/%** division entière du nombre à sa gauche par le nombre à sa droite, 13%/%3 = 4

Une liste de *fonctions génériques prédéfinies* (log, sqrt,...).

Pour obtenir ces informations complètes, utiliser la commande **help("Math")**.
    
Dans la console de RStudio, on essaie et on observe...


```R
# R comme calculatrice
2 + 3
3 - 2
2^3
13 %% 3
13 %/% 3
log(7) #fonction prédéfinie de R
runif(4)
```


5



1



8



1



4



1.94591014905531



<ol class=list-inline>
	<li>0.415537673747167</li>
	<li>0.683836687589064</li>
	<li>0.89679219131358</li>
	<li>0.928596392972395</li>
</ol>



## Programmation sous R - Espace de travail

Un des concepts de base en programmation est la **variable**.

Une variable vous permet de **stocker une valeur** (par exemple 4) ou **un objet** (par exemple fonction) à l’aide de **l’opérateur d’assignation**

   opérateur d'assignation `<-` dans R. 

Vous pouvez ensuite utiliser le nom de cette variable pour accéder facilement à la valeur ou à l'objet stocké dans cette variable.

On peut retrouver l'ensemble des objets stockés avec la fonction `ls()` dans la console ou dans l'onglet **Environment** de RStudio.

On essaie et on observe...



```R
# R comme langage interprete - espace de travail
ls()  ## rien pour l'instant
n <- 5
n
a <- runif(n)
a
b <- 10
nb <- n + b
nb
"(1) Espace de travail :"
ls()
rm(n)
# Espace de travail
"(2) Espace de travail :"
ls()
# Nettoyer l'espace de travail
rm(list = ls())
"(3) Espace de travail :"
ls()
```






5



<ol class=list-inline>
	<li>0.621818745741621</li>
	<li>0.352833890821785</li>
	<li>0.200256045674905</li>
	<li>0.445061427541077</li>
	<li>0.989104883745313</li>
</ol>




15



'(1) Espace de travail :'



<ol class=list-inline>
	<li>'a'</li>
	<li>'b'</li>
	<li>'n'</li>
	<li>'nb'</li>
</ol>




'(2) Espace de travail :'



<ol class=list-inline>
	<li>'a'</li>
	<li>'b'</li>
	<li>'nb'</li>
</ol>




'(3) Espace de travail :'






### Quelques statistiques


```R
# R comme langage interprete - statistiques
n <- 20
a <- runif(n)
mean(a)
sd(a)
summary(a)
```


0.527959211776033



0.257873986261877



       Min. 1st Qu.  Median    Mean 3rd Qu.    Max. 
     0.1362  0.3179  0.5495  0.5280  0.7143  0.9803 



```R
barplot(a)
```


![png](IntroR_files/IntroR_12_0.png)



```R
b <- rnorm(2000, mean = 0, sd = 1)
hist(b)
```


![png](IntroR_files/IntroR_13_0.png)



```R
# Les distributions du package de base
help(Distributions)
```

### Espace de travail (workspace)

L'espace de travail est l'ensemble des **objets** stockés, consultable:

* Dans la console par la fonction `ls()`.

* Dans l'onglet **"Environment"** de RStudio.


```R
# Espace de travail (workspace)
"(1) Espace de travail :"
ls()
a <- 7
"(2) Espace de travail :"
ls()
#
myfunc <- function() {
  y <- 1
  ls()
}
myfunc() 
# Effacer des objets 
rm(a)
"(3) Espace de travail :"
ls()
rm(myfunc)
"(4) Espace de travail :"
ls()
#
cmat1 <- matrix(1:20, ncol = 5)
cmat2 <- matrix(1:20, ncol = 4)
"(5) Espace de travail :"
ls()
# Effacer tout l'espace de travail
rm(list = ls())
"(6) Espace de travail :"
ls()
```


'(1) Espace de travail :'



<ol class=list-inline>
	<li>'a'</li>
	<li>'b'</li>
	<li>'n'</li>
</ol>




'(2) Espace de travail :'



<ol class=list-inline>
	<li>'a'</li>
	<li>'b'</li>
	<li>'n'</li>
</ol>




'y'



'(3) Espace de travail :'



<ol class=list-inline>
	<li>'b'</li>
	<li>'myfunc'</li>
	<li>'n'</li>
</ol>




'(4) Espace de travail :'



<ol class=list-inline>
	<li>'b'</li>
	<li>'n'</li>
</ol>




'(5) Espace de travail :'



<ol class=list-inline>
	<li>'b'</li>
	<li>'cmat1'</li>
	<li>'cmat2'</li>
	<li>'n'</li>
</ol>




'(6) Espace de travail :'







Pour en savoir plus: [Working Directories and Workspaces](https://support.rstudio.com/hc/en-us/articles/200711843-Working-Directories-and-Workspaces).

### Les types de base en R

R utilise différents types de données dont les principaux sont:

   * Un objet "vide" : `NULL`
   * Les *valeurs réelles* comme 4.5 sont appelées **numériques**.
   * Les *nombres naturels* comme 4 sont appelés *integer*. Les nombres entiers sont aussi **numériques**.
   * Les *valeurs booléennes* (TRUE ou FALSE) sont appelées **logiques**.
   * Les *chaînes de caractères* comme "nom" sont appelées **character**.
 
Les guillemets de part et d'autre d'un texte ("texte") indique que c'est du type **character**.

Pour connaître le mode d'un objet `x` de R, il suffit d'exécuter la fonction `mode(x)`.

* La fonction `typeof()` permet d'obtenir une description plus précise de la représentation interne d'un objet.

* R utilise un mécanisme de fonction générique simple, le choix de la méthode utilisée dépend de la classe du premier argument de la fonction générique (ex: `plot()`, `print()`, ...).

Pour connaître la classe d'une variable, on utilise la fonction `class()`.


```R
# Variables de differents types
my_logical   <- FALSE 
my_integer   <- 5L
my_numeric   <- 426
my_character <- "42"
```


```R
# mode, type et classe de my_numeric
mode(my_numeric)
typeof(my_numeric)
class(my_numeric)
```


'numeric'



'double'



'numeric'



```R
# mode, type et classe de my_numeric
mode(my_integer)
typeof(my_integer)
class(my_integer)

xx <- my_numeric + my_integer
xx
typeof(xx)
```


'numeric'



'integer'



'integer'



431



'double'



```R
# mode, type et classe de my_character
mode(my_character)
typeof(my_character)
class(my_character)
```


'character'



'character'



'character'



```R
# my_character + 10  # argument non numérique pour un opérateur binaire
paste(my_character, "euros") # Concatène après conversion en caractères
b <- paste(my_character, 14)
b
mode(b)
class(b)
```


'42 euros'



'42 14'



'character'



'character'



```R
# mode et classe de my_logical
mode(my_logical)
class(my_logical)
```


'logical'



'logical'


#### Tester/ concertir le mode d'un objet

* Il est possible de tester le **mode** d'un objet **x** de R avec les fonctions:
   - `is.null(x)`
   - `is.numeric(x)`
   - `is.logical(x)`
   - `is.character(x)`
   - `is.complex(x)`
   
Le résultat de ces fonctions est un booléen qui prend les valeurs `TRUE`, `FALSE` ou `NA` (missing value).

* Il est possible de convertir un objet d'un mode à un autre avec les fonctions:
   - `as.numeric(x)`
   - `as.logical(x)`
   - `as.character(x)`
   - `as.complex(x)`
   
Il faut rester prudent quant à la signification de ces conversions, R retourne toujours un résultat même si la conversion n'a pas de sens...


```R
# Conversion "logical" en "numeric"
a <- TRUE
mode(a)
d <- as.numeric(a)
mode(d)

# Conversion "logical" en "character"
as.character(a)

# Conversion "character" en "numeric"
b <- "3"
as.numeric(b)
6 + as.numeric(b)

```


'logical'



'numeric'



'TRUE'



3



9




#### Exemple de fonction générique 

La fonction **print**


```R
# fonction generique
a <- 1:20
print(a)

b <- 5.8
class(b)
print(b)

# objet de classe "matrix"
cmat1 <- matrix(a, ncol = 5)
class(cmat1)
print(cmat1)

cmat2 <- matrix(a, ncol = 4)
class(cmat2)
print(cmat2)

```

     [1]  1  2  3  4  5  6  7  8  9 10 11 12 13 14 15 16 17 18 19 20



'numeric'


    [1] 5.8



'matrix'


         [,1] [,2] [,3] [,4] [,5]
    [1,]    1    5    9   13   17
    [2,]    2    6   10   14   18
    [3,]    3    7   11   15   19
    [4,]    4    8   12   16   20



'matrix'


         [,1] [,2] [,3] [,4]
    [1,]    1    6   11   16
    [2,]    2    7   12   17
    [3,]    3    8   13   18
    [4,]    4    9   14   19
    [5,]    5   10   15   20


#### Valeur manquante et valeurs spéciales

* Certaines données sont **absentes** ou **inexploitables**, elles ne sont pas disponibles pour le traitement, R les note **NA** pour **Not Available**.

Pour savoir où se trouvent les données manquantes, on utilise la fonction `is.na(x)` qui retourne un vecteur de booléens de même longueur que x.

* On peut mentionner également  les valeurs spéciales `Inf` pour *l'infini* et `NaN` pour *Not a Number*.

et les fonctions de test associés:
    
- `is.finite(x)`
- `is.infinite(x)`
- `is.nan(x)`


```R
# donnees manquantes ou non
v <- c(2, 6, 4, 8, 2, 9, 10)
v
mean(v)

vv <- c(2, 6, 4, 8, 2, NA, 10)
mean(vv)
mean(vv, na.rm = TRUE)
```


<ol class=list-inline>
	<li>2</li>
	<li>6</li>
	<li>4</li>
	<li>8</li>
	<li>2</li>
	<li>9</li>
	<li>10</li>
</ol>




5.85714285714286



&lt;NA&gt;



5.33333333333333



```R
# Valeurs spéciales
exp(1e10)
w <- c(log(-4), 6, 9)
w
mean(w)
mean(w, na.rm = TRUE)
```


Inf


    Warning message in log(-4):
    “production de NaN”


<ol class=list-inline>
	<li>NaN</li>
	<li>6</li>
	<li>9</li>
</ol>




NaN



7.5


## Utilisation de scripts

Il n’est pas toujours très pratique de travailler uniquement sur la console, la saisie est réalisée ligne à ligne. En cas d’erreur de saisie, on devra soit resaisir la commande en la corrigeant, soit la rappeler en la recherchant dans l’historique des commandes en utilisant l'onglet **History** de RStudio pour pouvoir la modifier.

La méthode utilisée est de construire des scripts. Un script est un fichier texte qui contient une séquence d'instructions. La saisie de ces instructions peut se faire à l'extérieur de RStudio avec un éditeur (nedit, emacs, notepad,...) ou directement dans l'éditeur de RStudio.

Pour créer un nouveau script, utiliser le menu `File -> New File -> R Script`.

Une fenêtre d'édition s'ouvre dans laquelle vous pouvez saisir vos instructions, exécuter instruction par instruction (Ctrl + Entrée) ou globalement, sauvegarder le script, charger des scripts existants...

Pour en savoir plus: [Editing and Executing Code](https://support.rstudio.com/hc/en-us/articles/200484448-Editing-and-Executing-Code).


## Qu'est ce qu'un Data Frame?

**Un tableau de données est une liste de vecteurs (de même taille) rangés colonne par colonne**. Chaque colonne du tableau data correspond à une variable, chaque ligne à une observation. Les variables peuvent être de type différent (numérique, booléen, character, ...).

C'est ce même concept qu'on utilise dans les progiciels statistiques tels que SAS, BMDP, SPSS...

Sous R, les tableaux de données sont des objets particuliers appelés **data.frame**. On verra dans la suite du cours que tout type d'objet a deux attributs intrinsèques, **son mode** et **sa longueur**. Il peut
avoir des attributs spécifiques qui diffèrent selon le type de l'objet (dim, dimnames, class, ...).

Pour obtenir les attributs d'un objet, on utilise la fonction `attributes()`. Les attributs d'un **data-frame** sont le nom des variables (*names*), le nom des individus (*row.names*), les dimensions du tableau, le nombre des lignes et des colonnes.

Pour accéder à la valeur de ces attributs, on utilise `attr(<objet>,  <le nom de l'attribut>)`.

La fonction `dim()` fournit les dimensions du tableau de données: le nombre d'observations et le nombre de variables.

La fonction `dimnames()` fournit le nom des dimensions du tableau de données: le nom des individus et le nom des variables.


### Traitement du fichier de données "mtcars.csv"

* *Description* : les données ont été extraites du magazine Motor Trend US de 1974 et comprennent la consommation de carburant et 10 aspects de la conception et des performances de 32 automobiles (modèles 1973-1974).

* *Format* : 

Un **fichier de données** "mtcars" avec 32 *observations* sur 11 *variables*:

    mpg: Miles/(US) gallon (variable quantitative)
    cyl: Number of cylinders (variable qualitative)
    disp: Displacement (cu.in.) (variable quantitative)
    hp:	Gross horsepower (variable quantitative)
    drat: Rear axle ratio (variable quantitative)
    wt:	Weight (1000 lbs) (variable quantitative)
    qsec: 1/4 mile time (variable quantitative)
    vs: V/S (variable qualitative)
    am:	Transmission (0 = automatic, 1 = manual) (variable qualitative)
    gear: Number of forward gears (variable qualitative)
    carb: Number of carburetors (variable qualitative)
    
La première ligne du fichier de données (*mtcars.csv*) contient les labels des 11 variables et pour tout individu, l'observation de chaque variable, séparées par des virgules.

<img src="../figures/donMtcars.jpg",width="60%",height="60%">

On lit le fichier de données avec la fonction `read.csv()` adaptée au format qui fournit en sortie un objet de type **data.frame**. D'autres fonctions sont disponibles pour d'autres formats de données. Certains packages (**readr**, **data.table**) fournissent d'autres fonctions souvent plus rapides, pour les plus gros jeux de données.

Le fichier de données se trouve dans le répertoire **data**.


```R
help(read.csv)
```


```R
# Lecture du fichier de données
mtcars <- read.csv("data/mtcars.csv", header = TRUE, sep = ",")
ls()
```


<ol class=list-inline>
	<li>'a'</li>
	<li>'b'</li>
	<li>'cmat1'</li>
	<li>'cmat2'</li>
	<li>'d'</li>
	<li>'mtcars'</li>
	<li>'my_character'</li>
	<li>'my_integer'</li>
	<li>'my_logical'</li>
	<li>'my_numeric'</li>
	<li>'v'</li>
	<li>'vv'</li>
	<li>'w'</li>
	<li>'xx'</li>
</ol>




```R
class(mtcars)
attributes(mtcars)
names(mtcars)  # fonction pour recuperer l'attribut qui contient le nom des variables
attr(mtcars, "names")
```


'data.frame'



<dl>
	<dt>$names</dt>
		<dd><ol class=list-inline>
	<li>'mpg'</li>
	<li>'cyl'</li>
	<li>'disp'</li>
	<li>'hp'</li>
	<li>'drat'</li>
	<li>'wt'</li>
	<li>'qsec'</li>
	<li>'vs'</li>
	<li>'am'</li>
	<li>'gear'</li>
	<li>'carb'</li>
</ol>
</dd>
	<dt>$class</dt>
		<dd>'data.frame'</dd>
	<dt>$row.names</dt>
		<dd><ol class=list-inline>
	<li>1</li>
	<li>2</li>
	<li>3</li>
	<li>4</li>
	<li>5</li>
	<li>6</li>
	<li>7</li>
	<li>8</li>
	<li>9</li>
	<li>10</li>
	<li>11</li>
	<li>12</li>
	<li>13</li>
	<li>14</li>
	<li>15</li>
	<li>16</li>
	<li>17</li>
	<li>18</li>
	<li>19</li>
	<li>20</li>
	<li>21</li>
	<li>22</li>
	<li>23</li>
	<li>24</li>
	<li>25</li>
	<li>26</li>
	<li>27</li>
	<li>28</li>
	<li>29</li>
	<li>30</li>
	<li>31</li>
	<li>32</li>
</ol>
</dd>
</dl>




<ol class=list-inline>
	<li>'mpg'</li>
	<li>'cyl'</li>
	<li>'disp'</li>
	<li>'hp'</li>
	<li>'drat'</li>
	<li>'wt'</li>
	<li>'qsec'</li>
	<li>'vs'</li>
	<li>'am'</li>
	<li>'gear'</li>
	<li>'carb'</li>
</ol>




<ol class=list-inline>
	<li>'mpg'</li>
	<li>'cyl'</li>
	<li>'disp'</li>
	<li>'hp'</li>
	<li>'drat'</li>
	<li>'wt'</li>
	<li>'qsec'</li>
	<li>'vs'</li>
	<li>'am'</li>
	<li>'gear'</li>
	<li>'carb'</li>
</ol>




```R
mtcars
```


<table>
<thead><tr><th scope=col>mpg</th><th scope=col>cyl</th><th scope=col>disp</th><th scope=col>hp</th><th scope=col>drat</th><th scope=col>wt</th><th scope=col>qsec</th><th scope=col>vs</th><th scope=col>am</th><th scope=col>gear</th><th scope=col>carb</th></tr></thead>
<tbody>
	<tr><td>21.0 </td><td>6    </td><td>160.0</td><td>110  </td><td>3.90 </td><td>2.620</td><td>16.46</td><td>0    </td><td>1    </td><td>4    </td><td>4    </td></tr>
	<tr><td>21.0 </td><td>6    </td><td>160.0</td><td>110  </td><td>3.90 </td><td>2.875</td><td>17.02</td><td>0    </td><td>1    </td><td>4    </td><td>4    </td></tr>
	<tr><td>22.8 </td><td>4    </td><td>108.0</td><td> 93  </td><td>3.85 </td><td>2.320</td><td>18.61</td><td>1    </td><td>1    </td><td>4    </td><td>1    </td></tr>
	<tr><td>21.4 </td><td>6    </td><td>258.0</td><td>110  </td><td>3.08 </td><td>3.215</td><td>19.44</td><td>1    </td><td>0    </td><td>3    </td><td>1    </td></tr>
	<tr><td>18.7 </td><td>8    </td><td>360.0</td><td>175  </td><td>3.15 </td><td>3.440</td><td>17.02</td><td>0    </td><td>0    </td><td>3    </td><td>2    </td></tr>
	<tr><td>18.1 </td><td>6    </td><td>225.0</td><td>105  </td><td>2.76 </td><td>3.460</td><td>20.22</td><td>1    </td><td>0    </td><td>3    </td><td>1    </td></tr>
	<tr><td>14.3 </td><td>8    </td><td>360.0</td><td>245  </td><td>3.21 </td><td>3.570</td><td>15.84</td><td>0    </td><td>0    </td><td>3    </td><td>4    </td></tr>
	<tr><td>24.4 </td><td>4    </td><td>146.7</td><td> 62  </td><td>3.69 </td><td>3.190</td><td>20.00</td><td>1    </td><td>0    </td><td>4    </td><td>2    </td></tr>
	<tr><td>22.8 </td><td>4    </td><td>140.8</td><td> 95  </td><td>3.92 </td><td>3.150</td><td>22.90</td><td>1    </td><td>0    </td><td>4    </td><td>2    </td></tr>
	<tr><td>19.2 </td><td>6    </td><td>167.6</td><td>123  </td><td>3.92 </td><td>3.440</td><td>18.30</td><td>1    </td><td>0    </td><td>4    </td><td>4    </td></tr>
	<tr><td>17.8 </td><td>6    </td><td>167.6</td><td>123  </td><td>3.92 </td><td>3.440</td><td>18.90</td><td>1    </td><td>0    </td><td>4    </td><td>4    </td></tr>
	<tr><td>16.4 </td><td>8    </td><td>275.8</td><td>180  </td><td>3.07 </td><td>4.070</td><td>17.40</td><td>0    </td><td>0    </td><td>3    </td><td>3    </td></tr>
	<tr><td>17.3 </td><td>8    </td><td>275.8</td><td>180  </td><td>3.07 </td><td>3.730</td><td>17.60</td><td>0    </td><td>0    </td><td>3    </td><td>3    </td></tr>
	<tr><td>15.2 </td><td>8    </td><td>275.8</td><td>180  </td><td>3.07 </td><td>3.780</td><td>18.00</td><td>0    </td><td>0    </td><td>3    </td><td>3    </td></tr>
	<tr><td>10.4 </td><td>8    </td><td>472.0</td><td>205  </td><td>2.93 </td><td>5.250</td><td>17.98</td><td>0    </td><td>0    </td><td>3    </td><td>4    </td></tr>
	<tr><td>10.4 </td><td>8    </td><td>460.0</td><td>215  </td><td>3.00 </td><td>5.424</td><td>17.82</td><td>0    </td><td>0    </td><td>3    </td><td>4    </td></tr>
	<tr><td>14.7 </td><td>8    </td><td>440.0</td><td>230  </td><td>3.23 </td><td>5.345</td><td>17.42</td><td>0    </td><td>0    </td><td>3    </td><td>4    </td></tr>
	<tr><td>32.4 </td><td>4    </td><td> 78.7</td><td> 66  </td><td>4.08 </td><td>2.200</td><td>19.47</td><td>1    </td><td>1    </td><td>4    </td><td>1    </td></tr>
	<tr><td>30.4 </td><td>4    </td><td> 75.7</td><td> 52  </td><td>4.93 </td><td>1.615</td><td>18.52</td><td>1    </td><td>1    </td><td>4    </td><td>2    </td></tr>
	<tr><td>33.9 </td><td>4    </td><td> 71.1</td><td> 65  </td><td>4.22 </td><td>1.835</td><td>19.90</td><td>1    </td><td>1    </td><td>4    </td><td>1    </td></tr>
	<tr><td>21.5 </td><td>4    </td><td>120.1</td><td> 97  </td><td>3.70 </td><td>2.465</td><td>20.01</td><td>1    </td><td>0    </td><td>3    </td><td>1    </td></tr>
	<tr><td>15.5 </td><td>8    </td><td>318.0</td><td>150  </td><td>2.76 </td><td>3.520</td><td>16.87</td><td>0    </td><td>0    </td><td>3    </td><td>2    </td></tr>
	<tr><td>15.2 </td><td>8    </td><td>304.0</td><td>150  </td><td>3.15 </td><td>3.435</td><td>17.30</td><td>0    </td><td>0    </td><td>3    </td><td>2    </td></tr>
	<tr><td>13.3 </td><td>8    </td><td>350.0</td><td>245  </td><td>3.73 </td><td>3.840</td><td>15.41</td><td>0    </td><td>0    </td><td>3    </td><td>4    </td></tr>
	<tr><td>19.2 </td><td>8    </td><td>400.0</td><td>175  </td><td>3.08 </td><td>3.845</td><td>17.05</td><td>0    </td><td>0    </td><td>3    </td><td>2    </td></tr>
	<tr><td>27.3 </td><td>4    </td><td> 79.0</td><td> 66  </td><td>4.08 </td><td>1.935</td><td>18.90</td><td>1    </td><td>1    </td><td>4    </td><td>1    </td></tr>
	<tr><td>26.0 </td><td>4    </td><td>120.3</td><td> 91  </td><td>4.43 </td><td>2.140</td><td>16.70</td><td>0    </td><td>1    </td><td>5    </td><td>2    </td></tr>
	<tr><td>30.4 </td><td>4    </td><td> 95.1</td><td>113  </td><td>3.77 </td><td>1.513</td><td>16.90</td><td>1    </td><td>1    </td><td>5    </td><td>2    </td></tr>
	<tr><td>15.8 </td><td>8    </td><td>351.0</td><td>264  </td><td>4.22 </td><td>3.170</td><td>14.50</td><td>0    </td><td>1    </td><td>5    </td><td>4    </td></tr>
	<tr><td>19.7 </td><td>6    </td><td>145.0</td><td>175  </td><td>3.62 </td><td>2.770</td><td>15.50</td><td>0    </td><td>1    </td><td>5    </td><td>6    </td></tr>
	<tr><td>15.0 </td><td>8    </td><td>301.0</td><td>335  </td><td>3.54 </td><td>3.570</td><td>14.60</td><td>0    </td><td>1    </td><td>5    </td><td>8    </td></tr>
	<tr><td>21.4 </td><td>4    </td><td>121.0</td><td>109  </td><td>4.11 </td><td>2.780</td><td>18.60</td><td>1    </td><td>1    </td><td>4    </td><td>2    </td></tr>
</tbody>
</table>




```R
# variable disp dans le data-frame "mtcars"
mtcars[, "disp"]  ## matrix-like accessor
mtcars[["disp"]]  ## list-like accessor
```


<ol class=list-inline>
	<li>160</li>
	<li>160</li>
	<li>108</li>
	<li>258</li>
	<li>360</li>
	<li>225</li>
	<li>360</li>
	<li>146.7</li>
	<li>140.8</li>
	<li>167.6</li>
	<li>167.6</li>
	<li>275.8</li>
	<li>275.8</li>
	<li>275.8</li>
	<li>472</li>
	<li>460</li>
	<li>440</li>
	<li>78.7</li>
	<li>75.7</li>
	<li>71.1</li>
	<li>120.1</li>
	<li>318</li>
	<li>304</li>
	<li>350</li>
	<li>400</li>
	<li>79</li>
	<li>120.3</li>
	<li>95.1</li>
	<li>351</li>
	<li>145</li>
	<li>301</li>
	<li>121</li>
</ol>




<ol class=list-inline>
	<li>160</li>
	<li>160</li>
	<li>108</li>
	<li>258</li>
	<li>360</li>
	<li>225</li>
	<li>360</li>
	<li>146.7</li>
	<li>140.8</li>
	<li>167.6</li>
	<li>167.6</li>
	<li>275.8</li>
	<li>275.8</li>
	<li>275.8</li>
	<li>472</li>
	<li>460</li>
	<li>440</li>
	<li>78.7</li>
	<li>75.7</li>
	<li>71.1</li>
	<li>120.1</li>
	<li>318</li>
	<li>304</li>
	<li>350</li>
	<li>400</li>
	<li>79</li>
	<li>120.3</li>
	<li>95.1</li>
	<li>351</li>
	<li>145</li>
	<li>301</li>
	<li>121</li>
</ol>




```R
mtcars[1:4, ] # 4 premieres observations de toutes les variable
```


<table>
<thead><tr><th scope=col>mpg</th><th scope=col>cyl</th><th scope=col>disp</th><th scope=col>hp</th><th scope=col>drat</th><th scope=col>wt</th><th scope=col>qsec</th><th scope=col>vs</th><th scope=col>am</th><th scope=col>gear</th><th scope=col>carb</th></tr></thead>
<tbody>
	<tr><td>21.0 </td><td>6    </td><td>160  </td><td>110  </td><td>3.90 </td><td>2.620</td><td>16.46</td><td>0    </td><td>1    </td><td>4    </td><td>4    </td></tr>
	<tr><td>21.0 </td><td>6    </td><td>160  </td><td>110  </td><td>3.90 </td><td>2.875</td><td>17.02</td><td>0    </td><td>1    </td><td>4    </td><td>4    </td></tr>
	<tr><td>22.8 </td><td>4    </td><td>108  </td><td> 93  </td><td>3.85 </td><td>2.320</td><td>18.61</td><td>1    </td><td>1    </td><td>4    </td><td>1    </td></tr>
	<tr><td>21.4 </td><td>6    </td><td>258  </td><td>110  </td><td>3.08 </td><td>3.215</td><td>19.44</td><td>1    </td><td>0    </td><td>3    </td><td>1    </td></tr>
</tbody>
</table>




```R
names(mtcars)
row.names(mtcars)
```


<ol class=list-inline>
	<li>'mpg'</li>
	<li>'cyl'</li>
	<li>'disp'</li>
	<li>'hp'</li>
	<li>'drat'</li>
	<li>'wt'</li>
	<li>'qsec'</li>
	<li>'vs'</li>
	<li>'am'</li>
	<li>'gear'</li>
	<li>'carb'</li>
</ol>




<ol class=list-inline>
	<li>'1'</li>
	<li>'2'</li>
	<li>'3'</li>
	<li>'4'</li>
	<li>'5'</li>
	<li>'6'</li>
	<li>'7'</li>
	<li>'8'</li>
	<li>'9'</li>
	<li>'10'</li>
	<li>'11'</li>
	<li>'12'</li>
	<li>'13'</li>
	<li>'14'</li>
	<li>'15'</li>
	<li>'16'</li>
	<li>'17'</li>
	<li>'18'</li>
	<li>'19'</li>
	<li>'20'</li>
	<li>'21'</li>
	<li>'22'</li>
	<li>'23'</li>
	<li>'24'</li>
	<li>'25'</li>
	<li>'26'</li>
	<li>'27'</li>
	<li>'28'</li>
	<li>'29'</li>
	<li>'30'</li>
	<li>'31'</li>
	<li>'32'</li>
</ol>




```R
dimnames(mtcars)
```


<ol>
	<li><ol class=list-inline>
	<li>'1'</li>
	<li>'2'</li>
	<li>'3'</li>
	<li>'4'</li>
	<li>'5'</li>
	<li>'6'</li>
	<li>'7'</li>
	<li>'8'</li>
	<li>'9'</li>
	<li>'10'</li>
	<li>'11'</li>
	<li>'12'</li>
	<li>'13'</li>
	<li>'14'</li>
	<li>'15'</li>
	<li>'16'</li>
	<li>'17'</li>
	<li>'18'</li>
	<li>'19'</li>
	<li>'20'</li>
	<li>'21'</li>
	<li>'22'</li>
	<li>'23'</li>
	<li>'24'</li>
	<li>'25'</li>
	<li>'26'</li>
	<li>'27'</li>
	<li>'28'</li>
	<li>'29'</li>
	<li>'30'</li>
	<li>'31'</li>
	<li>'32'</li>
</ol>
</li>
	<li><ol class=list-inline>
	<li>'mpg'</li>
	<li>'cyl'</li>
	<li>'disp'</li>
	<li>'hp'</li>
	<li>'drat'</li>
	<li>'wt'</li>
	<li>'qsec'</li>
	<li>'vs'</li>
	<li>'am'</li>
	<li>'gear'</li>
	<li>'carb'</li>
</ol>
</li>
</ol>




```R
summary(mtcars)
```


          mpg             cyl             disp             hp       
     Min.   :10.40   Min.   :4.000   Min.   : 71.1   Min.   : 52.0  
     1st Qu.:15.43   1st Qu.:4.000   1st Qu.:120.8   1st Qu.: 96.5  
     Median :19.20   Median :6.000   Median :196.3   Median :123.0  
     Mean   :20.09   Mean   :6.188   Mean   :230.7   Mean   :146.7  
     3rd Qu.:22.80   3rd Qu.:8.000   3rd Qu.:326.0   3rd Qu.:180.0  
     Max.   :33.90   Max.   :8.000   Max.   :472.0   Max.   :335.0  
          drat             wt             qsec             vs        
     Min.   :2.760   Min.   :1.513   Min.   :14.50   Min.   :0.0000  
     1st Qu.:3.080   1st Qu.:2.581   1st Qu.:16.89   1st Qu.:0.0000  
     Median :3.695   Median :3.325   Median :17.71   Median :0.0000  
     Mean   :3.597   Mean   :3.217   Mean   :17.85   Mean   :0.4375  
     3rd Qu.:3.920   3rd Qu.:3.610   3rd Qu.:18.90   3rd Qu.:1.0000  
     Max.   :4.930   Max.   :5.424   Max.   :22.90   Max.   :1.0000  
           am              gear            carb      
     Min.   :0.0000   Min.   :3.000   Min.   :1.000  
     1st Qu.:0.0000   1st Qu.:3.000   1st Qu.:2.000  
     Median :0.0000   Median :4.000   Median :2.000  
     Mean   :0.4062   Mean   :3.688   Mean   :2.812  
     3rd Qu.:1.0000   3rd Qu.:4.000   3rd Qu.:4.000  
     Max.   :1.0000   Max.   :5.000   Max.   :8.000  



```R
ls()
```


<ol class=list-inline>
	<li>'a'</li>
	<li>'b'</li>
	<li>'cmat1'</li>
	<li>'cmat2'</li>
	<li>'d'</li>
	<li>'mtcars'</li>
	<li>'my_character'</li>
	<li>'my_integer'</li>
	<li>'my_logical'</li>
	<li>'my_numeric'</li>
	<li>'v'</li>
	<li>'vv'</li>
	<li>'w'</li>
	<li>'xx'</li>
</ol>



### Sauvegarde

Il est possible de sauvegarder des données et/ou des résultats au format **RData**, format propre au logiciel R. Cela facilite l’archivage de résultats intermédiaires ou l’enregistrement d’un tableau de données nettoyé (recodage de 
valeurs manquantes, modification des modalités de variables qualitatives, correction des erreurs de saisie, ...) ou augmenté de variables auxiliaires. Pour cela, on utilise la commande **save()**. La commande **load()** permet de recharger des données sauvegardées au format **RData**. 


```R
# Contenu de l'espace de travail
ls()
save(mtcars, file = "data/mtcars.RData")
```


<ol class=list-inline>
	<li>'a'</li>
	<li>'b'</li>
	<li>'cmat1'</li>
	<li>'cmat2'</li>
	<li>'d'</li>
	<li>'mtcars'</li>
	<li>'my_character'</li>
	<li>'my_integer'</li>
	<li>'my_logical'</li>
	<li>'my_numeric'</li>
	<li>'v'</li>
	<li>'vv'</li>
	<li>'w'</li>
	<li>'xx'</li>
</ol>



** Load**


```R
rm(list = ls())
"(1) espace de travail :"
ls()
load("data/mtcars.RData")
"(2) espace de travail :"
ls()
```


'(1) espace de travail :'







'(2) espace de travail :'



'mtcars'


Le format **RDS** peut etre aussi utilisé pour stocker des objets R. Il a le bénéfice de pouvoir choisir le nom de la variable dans lequel mettre l'objet.


```R
saveRDS(mtcars, file = "data/mtcars.rds")
my_mtcars_name <- readRDS("data/mtcars.rds")
ls()
```


<ol class=list-inline>
	<li>'mtcars'</li>
	<li>'my_mtcars_name'</li>
</ol>



## Quitter R

Pour quitter **R**, exécuter la fonction 

   **q()**   # R vous demande
   
   **Save workspace image?** [y/n/c]: 

Si vous répondez "**y**" , vous sauvegardez la session, R crée deux  fichiers dans le répertoire de travail, **.Rhistory** et **.RData**.

   * **.Rhistory**:  historique des commandes de la session
   * **.RData**:  environnement (workspace) de la session.
    
  Attention !!! Nécessité de gérer le volume de ces sauvegardes...
  
Lorsque vous réouvrez une session, vous retrouvez l'environnement et l'historique de la session sauvegardée, ce qui peut dans de nombreux cas générer des erreurs pas toujours faciles à identifier.

On peut dans la configuration de RStudio (menu Preferences) demander à supprimer la sauvegarde automatique, ou demander à ce que la question soit posée. Si vous voulez tendre vers une recherche plus reproductible, il est conseillé de ne pas enregistrer votre environnement, votre code devrait être capable de le regénérer pour vous.

## Bibliothèques ou package

Un package ou une bibliothèque de programmes externes, est un ensemble de fonctions R, qui complète les fonctionnalités de R. 

* Liste des package installés


```R
library()
mat <- available.packages() # Packages disponibles (au CRAN)
nrow(mat)
```

    Warning message in library():
    “libraries ‘/usr/local/lib/R/site-library’, ‘/usr/lib/R/site-library’ contain no packages”




13224


* Charger un package(si la librairie est déjà chargée, ne fait rien)


```R
library(grDevices)
sessionInfo()     # packages chargés en cours de session
```


    R version 3.4.4 (2018-03-15)
    Platform: x86_64-pc-linux-gnu (64-bit)
    Running under: Linux Mint 18.3
    
    Matrix products: default
    BLAS: /usr/lib/openblas-base/libblas.so.3
    LAPACK: /usr/lib/libopenblasp-r0.2.18.so
    
    locale:
     [1] LC_CTYPE=fr_FR.UTF-8       LC_NUMERIC=C              
     [3] LC_TIME=fr_FR.UTF-8        LC_COLLATE=fr_FR.UTF-8    
     [5] LC_MONETARY=fr_FR.UTF-8    LC_MESSAGES=fr_FR.UTF-8   
     [7] LC_PAPER=fr_FR.UTF-8       LC_NAME=C                 
     [9] LC_ADDRESS=C               LC_TELEPHONE=C            
    [11] LC_MEASUREMENT=fr_FR.UTF-8 LC_IDENTIFICATION=C       
    
    attached base packages:
    [1] stats     graphics  grDevices utils     datasets  methods   base     
    
    loaded via a namespace (and not attached):
     [1] compiler_3.4.4       IRdisplay_0.6.1      pbdZMQ_0.3-3        
     [4] tools_3.4.4          htmltools_0.3.6      base64enc_0.1-3     
     [7] rstudioapi_0.7       crayon_1.3.4         Rcpp_1.0.0          
    [10] uuid_0.1-2           IRkernel_0.8.14.9000 jsonlite_1.5        
    [13] digest_0.6.18        repr_0.17            RhpcBLASctl_0.18-185
    [16] evaluate_0.12       


* Liste des fonctions d'un package


```R
library(help = "stats")  # fonctions du package "stats"
```



* Liste des datasets disponibles comme exemples sous R


```R
data()
data(package = "datasets") # datasets pour le package datasets
```

### Installation d'un package

               install.packages("gplots")

### Mise à jour d'un package

Certains packages sont en constante évolution avec de nouvelles versions disponibles. Il est préférable de les mettre à jour de temps en temps.

                 update.packages()

**Attention, la mise à jour d'un package est fonction de la version de R**. Il faudra mettre à jour R régulièrement. 

### Créer ses propres packages

L'utilisateur peut créer ses propres packages ([R avancé](https://privefl.github.io/advr38book/packages.html)), les diffuser sur le [CRAN](https://cran.r-project.org) ou pour l'usage de sa propre communauté.

### Installer un package en local

Utilisation de la variable d'environnement **R_LIBS** qui fournit le/les répertoire(s) d'installation en local des packages

            export R_LIBS="/home/yourusername/R/lib
            install.packages("gplots") # en local dans R_LIBS
            


```R
.libPaths()  # liste des repertoires de recherche des librairies
```


<ol class=list-inline>
	<li>'/home/privef/R/x86_64-pc-linux-gnu-library/3.4'</li>
	<li>'/usr/local/lib/R/site-library'</li>
	<li>'/usr/lib/R/site-library'</li>
	<li>'/usr/lib/R/library'</li>
</ol>



Si vous ne voulez pas mettre à jour la variable à chaque installation.

* Placer la commande *export* dans le fichier **.bashrc** de votre "home directory", elle sera exécutée à chaque session.

ou, 

* dans un fichier **.Renviron** de votre "home directory", la ligne

     *R_LIBS="/home/yourusername/R/lib"*

Vous pouvez indiquez plusieurs répertoires, séparés par **":"** 

* Pour indiquer la localisation des packages sur la plateforme utilisée, utiliser la variable **R_LIBS_SITE**

    *R_LIBS_SITE=/Library/Frameworks/R.framework/Versions/Current/Resources/library*
    

* Exécuter la fonction **.libPaths()** et observez. 


```R
.libPaths()  # liste des repertoires de recherche des librairies
```


<ol class=list-inline>
	<li>'/home/privef/R/x86_64-pc-linux-gnu-library/3.4'</li>
	<li>'/usr/local/lib/R/site-library'</li>
	<li>'/usr/lib/R/site-library'</li>
	<li>'/usr/lib/R/library'</li>
</ol>



* Ajouter un répertoire local

   *.libPaths("<path du repertoire local>")*
            
Plusieurs répertoires possibles, séparés par ":".


```R
.libPaths("/home/viryl/R/lib") # ajout d'un repertoire d'installation local
```


```R
.libPaths()
```


<ol class=list-inline>
	<li>'/usr/local/lib/R/site-library'</li>
	<li>'/usr/lib/R/site-library'</li>
	<li>'/usr/lib/R/library'</li>
</ol>



## Documentation 

1. Dans la console:
    - La fonction **help(ma_function)** ou **?ma_fonction** dans la console affiche l'aide dans l'onglet **Help** de l'interface RStudio. On y trouve:
    
        * la description de la fonction,
        * les arguments optionnels ou obligatoires utilisés dans cette fonction, leur signification,
        * les sorties de la fonction,
        * les références,
        * quelques exemples
    


```R
help(plot) # ou ?plot donne le même résultat. 
```

   - **Sur les détails de programmation** 
           


```R
help("[[") # encadré d'une simple ou double quote
```


```R
help("if"); help("for")
```

   - La fonction **help.start()** permet d'ouvrir la version online (HTML) de l'aide R.
   - Si on ne connait pas exactement le nom d'une fonction que l'on souhaite utiliser, il est alors possible de retrouver cette fonction grâce à **help.search("mot_clé")** qui vous affichera une liste des fonctions en rapport avec le mot_clé.
   - Si on ne se souvient que d'une partie du nom d'une fonction, on peut alors utiliser la fonction **apropos("pattern")** pour lister les noms des fonctions qui contiennent ce pattern.
   - Si aucune de ces fonctions n'a permis de résoudre votre problème, il reste la fonction **RSiteSearch("mot_clé1 mot_clé2 ...")** qui permet de faire directement une recherche dans la "R-help mailing list" (sorte de forum de discussion dédié à l'utilisation de R) ainsi que dans toutes les documentations de R et affiche les résultats dans une page Web.
   
2. On accède à une aide complète sur l'utilisation, l'installation de R dans l'onglet **Help** de **RStudio**.

3. De nombreux tutoriaux sur internet. Demandez à notre ami Google.


```R
help.search("correlation")
```




```R
apropos("NA")
```


<ol class=list-inline>
	<li>'addNA'</li>
	<li>'all.names'</li>
	<li>'allNames'</li>
	<li>'anyNA'</li>
	<li>'anyNA.numeric_version'</li>
	<li>'anyNA.POSIXlt'</li>
	<li>'as.name'</li>
	<li>'asNamespace'</li>
	<li>'aspell_write_personal_dictionary_file'</li>
	<li>'assignInMyNamespace'</li>
	<li>'assignInNamespace'</li>
	<li>'attachNamespace'</li>
	<li>'basename'</li>
	<li>'.BaseNamespaceEnv'</li>
	<li>'case.names'</li>
	<li>'citeNatbib'</li>
	<li>'classMetaName'</li>
	<li>'className'</li>
	<li>'colnames'</li>
	<li>'colnames&lt;-'</li>
	<li>'defaultDumpName'</li>
	<li>'determinant'</li>
	<li>'determinant.matrix'</li>
	<li>'dimnames'</li>
	<li>'dimnames&lt;-'</li>
	<li>'dimnames&lt;-.data.frame'</li>
	<li>'dimnames.data.frame'</li>
	<li>'dirname'</li>
	<li>'DNase'</li>
	<li>'.DollarNames'</li>
	<li>'elNamed'</li>
	<li>'elNamed&lt;-'</li>
	<li>'enc2native'</li>
	<li>'environmentName'</li>
	<li>'.External'</li>
	<li>'.External2'</li>
	<li>'.External.graphics'</li>
	<li>'externalRefMethod'</li>
	<li>'factanal'</li>
	<li>'file.rename'</li>
	<li>'fileSnapshot'</li>
	<li>'finalDefaultMethod'</li>
	<li>'findMethodSignatures'</li>
	<li>'fixInNamespace'</li>
	<li>'getClassName'</li>
	<li>'getFromNamespace'</li>
	<li>'getNamespace'</li>
	<li>'.getNamespace'</li>
	<li>'..getNamespace'</li>
	<li>'getNamespaceExports'</li>
	<li>'getNamespaceImports'</li>
	<li>'getNamespaceInfo'</li>
	<li>'.getNamespaceInfo'</li>
	<li>'getNamespaceName'</li>
	<li>'getNamespaceUsers'</li>
	<li>'getNamespaceVersion'</li>
	<li>'getNativeSymbolInfo'</li>
	<li>'getPackageName'</li>
	<li>'getSrcFilename'</li>
	<li>'getTaskCallbackNames'</li>
	<li>'hasName'</li>
	<li>'inheritedSlotNames'</li>
	<li>'.Internal'</li>
	<li>'isBaseNamespace'</li>
	<li>'is.na'</li>
	<li>'is.na&lt;-'</li>
	<li>'is.na.data.frame'</li>
	<li>'is.na&lt;-.default'</li>
	<li>'is.na&lt;-.factor'</li>
	<li>'is.name'</li>
	<li>'isNamespace'</li>
	<li>'isNamespaceLoaded'</li>
	<li>'is.nan'</li>
	<li>'is.na&lt;-.numeric_version'</li>
	<li>'is.na.numeric_version'</li>
	<li>'is.na.POSIXlt'</li>
	<li>'kernapply'</li>
	<li>'library.dynam'</li>
	<li>'library.dynam.unload'</li>
	<li>'loadedNamespaces'</li>
	<li>'loadingNamespaceInfo'</li>
	<li>'loadNamespace'</li>
	<li>'make.names'</li>
	<li>'matchSignature'</li>
	<li>'metaNameUndo'</li>
	<li>'methodSignatureMatrix'</li>
	<li>'methodsPackageMetaName'</li>
	<li>'mlistMetaName'</li>
	<li>'month.name'</li>
	<li>'my_mtcars_name'</li>
	<li>'na.action'</li>
	<li>'na.contiguous'</li>
	<li>'na.exclude'</li>
	<li>'na.fail'</li>
	<li>'names'</li>
	<li>'names&lt;-'</li>
	<li>'namespaceExport'</li>
	<li>'namespaceImport'</li>
	<li>'namespaceImportClasses'</li>
	<li>'namespaceImportFrom'</li>
	<li>'namespaceImportMethods'</li>
	<li>'names&lt;-.POSIXlt'</li>
	<li>'names.POSIXlt'</li>
	<li>'na.omit'</li>
	<li>'na.pass'</li>
	<li>'napredict'</li>
	<li>'naprint'</li>
	<li>'naresid'</li>
	<li>'nargs'</li>
	<li>'occupationalStatus'</li>
	<li>'OlsonNames'</li>
	<li>'packageHasNamespace'</li>
	<li>'packageName'</li>
	<li>'parseNamespaceFile'</li>
	<li>'print.NativeRoutineList'</li>
	<li>'provideDimnames'</li>
	<li>'reg.finalizer'</li>
	<li>'requireNamespace'</li>
	<li>'rownames'</li>
	<li>'row.names'</li>
	<li>'row.names&lt;-'</li>
	<li>'rownames&lt;-'</li>
	<li>'row.names&lt;-.data.frame'</li>
	<li>'row.names.data.frame'</li>
	<li>'row.names&lt;-.default'</li>
	<li>'row.names.default'</li>
	<li>'setNames'</li>
	<li>'setNamespaceInfo'</li>
	<li>'setPackageName'</li>
	<li>'signalCondition'</li>
	<li>'.signalSimpleWarning'</li>
	<li>'signature'</li>
	<li>'SignatureMethod'</li>
	<li>'slotNames'</li>
	<li>'.slotNames'</li>
	<li>'state.name'</li>
	<li>'substituteFunctionArgs'</li>
	<li>'taskCallbackManager'</li>
	<li>'unloadNamespace'</li>
	<li>'unname'</li>
	<li>'USPersonalExpenditure'</li>
	<li>'validSlotNames'</li>
	<li>'variable.names'</li>
</ol>



### Bibliographie

#### Sites Web

* [Site RStudio](https://www.rstudio.com/online-learning)
* [Using RStudio IDE](https://support.rstudio.com/hc/en-us/sections/200107586-Using-the-RStudio-IDE)
* [Introduction à R (MOOC - Introduction à la statistique avec R](https://www.fun-mooc.fr/c4x/UPSUD/42001S03/asset/introR.html)
* [Introduction à R et au tidyverse](https://juba.github.io/tidyverse/index.html)
* [Cours complet sur l'analyse de données (MOOC)](http://factominer.free.fr/course/MOOC_fr.html)
* [Data Science](https://www.rstudio.com/online-learning/#DataScience)
* [Télécharger des Aide-Mémoires sur le site de RStudio](https://www.rstudio.com/resources/cheatsheets/)

#### Livres

* [Hands-On Programming with R - Write Your Own Functions and Simulations (Garrett Grolemund)](http://shop.oreilly.com/product/0636920028574.do#)
* [Training books](https://www.rstudio.com/resources/training/books/)
