# Exercice 1
# -----------
# Fusion et Selection
# Les donnees sont dans TP/data
setwd("~/formation/cours-stat-R-2019/IntroR/ced_intror/TP/data")

# importation des deux fichiers: fusion1.csv et fusion2.csv
fusion1 <- read.csv("fusion1.csv",sep=";",dec=",",header = TRUE)
fusion2 <- read.csv("fusion2.csv",sep=";",dec=",",header = TRUE)
names(fusion1)
summary(fusion1)
dim(fusion1)
names(fusion2)
summary(fusion2)
dim(fusion2)

# Conserver les variables yhat1, yhat3, Rhamnos, Arabinos
fusion12 <- fusion1[,c("yhat1","yhat3")]
fusion22 <- fusion2[,c("Rhamnos","Arabinos")]
names(fusion12)
names(fusion22)
don <- cbind(fusion12,fusion22)
names(don)
dim(don)

# ajouter a don yres1 et yres2
yres1 <- don[,"yhat1"]-don[,"Rhamnos"]
yres2 <- don[,"yhat3"] - don[,"Arabinos"]

don <- cbind.data.frame(don,yres1,yres2)
names(don)
dim(don)