# Exercices Gestion de données
# Ventilation
# Creation de la variable qualitative
Xqual <- factor(c(rep("A",60),rep("B",20),rep("C",17),rep("D",3)))

# Frequence de chaque modalite
tabl <- table(Xqual)
freq <-tabl/sum(tabl)

# Modalites dont l'effectif est inferieur a 5%
modalites <- levels(Xqual)
select <- freq < 0.05
modalites[select]  # names(which(select))

# frequences des modalites sans la modalite "D"
lesquels <- modalites[!select]
prov <- factor(Xqual[Xqual%in%lesquels])
tabprov <- table(prov)
freqprov <- tabprov/sum(tabprov)
