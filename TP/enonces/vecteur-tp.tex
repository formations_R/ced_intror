\documentclass[12pt,twoside,openright]{article}

%
% Pour les documents rédiges en Français
%
\usepackage{ucs}
\usepackage[utf8]{inputenc}

% Utilisation des commandes d'indentation
%
\usepackage{indentfirst}

%
% Style de Page
% --------------------
%
\pagestyle{headings}

%\setlength{textheight}{8.0in}

% Pour rentrer des sources d'algorithmes
\usepackage{minted}

% Graphics
% -------------
\usepackage[dvips]{graphicx}

% Ajout de commentaires
%
\usepackage{comment}

% Bibliographie
% -------------------
%  \bibliographiestyle{}

% Titre, auteur et date
% ---------------------------
\title{\normalsize MaiMoSiNE/Collège doctoral UGA \\
\vspace{0.3cm}
   Les objets R}
   
\date{\normalsize du 19 Novembre au 12 Décembre 2018}

% Pied de page
% -------------------
\usepackage{fancyheadings}
%\pagestyle{fancy}

% Debut du document
%
\begin{document}
\maketitle
%
 \noindent \textbf{Exercice 1}:
 
 \begin{itemize}
\item \'Evaluer les expressions  TRUE+FALSE, TRUE+FALSE+TRUE*4 et donner le mode et la classe des résulats.
\item Évaluer les expressions suivantes : c(1, 4, TRUE), et c(1, 4, TRUE, "bonjour") et donner le mode et la classe des résulats.
\end{itemize}
%
 \noindent \textbf{Exercice 2}: Manipulation de vecteurs

\begin{itemize}
\item Créer les vecteurs  \textbf{y1}, \textbf{y2}, \textbf{y3} et \textbf{y4}, avec $d = 4$ et $e = 12$.
	\begin{itemize}
	\item y1 : une suite d'indices de 1 à 12.
	\item y2 : trois fois l'élément $d$, puis trois fois d au carré, puis trois fois la racine de d.
	\item y3 : la séquence de 1 à 20 avec un pas de deux.
	\item y4 : 10 chiffres compris entre 1 et 30 avec un intervalle constant.
	\end{itemize}
\item Les principales fonctions mathématiques s'appliquent sur les scalaires, vecteurs, matrices,...
	\begin{itemize}
	\item log()/log10() Logarithme népérien/décimal
	\item exp() Exponentielle.
	\item cos()/sin()/tan() Cosinus/Sinus/Tangente.
	\item abs() Valeur absolue.
	\item sqrt() Racine carrée.
	\end{itemize}
	Observer avec les vecteurs précédemment construits.
%\end{itemize}
\item Manipulation de vecteurs
	\begin{itemize}
	\item Afficher le mode et la classe de y1 et y2, puis leur longueur ;
	\item Extraire le premier élément de y2, puis le dernier ;
	\item Extraire les trois premier éléments de y2 et les stocker dans un vecteur que l’on nommera z ;
	\item Extraire les éléments en position 1, 3, 5 de y2 et les stocker dans un vecteur que l’on nommera w;
	\item Additionner le nombre 4 au vecteur y2, puis multipliser le résultat par 5 ;
	\item Effectuer l’addition de z et w, commenter le résultat ;
	\item Effectuer l’addition suivante : y2+z, commenter le résultat, puis regarder le résultat de z+y2 ;
	\item  Multiplier y2 par le scalaire c que l’on fixera à 3;
	\item Effectuer la multiplication de z et w, commenter le résultat ;
	\item Effectuer la multiplication suivante : y2*z, commenter le résultat ;
	\item Récupérer les positions des multiples de 2 dans y1 et les stocker dans un vecteur que l’on nommera \textbf{ind}, puis conserver uniquement les multiples de 2 de y2 dans un vecteur que l’on nommera \textbf{mult$\_2$} ;
	\item Afficher les éléments de y1 qui sont multiples de 3 et multiples de 2 ;
	\item Calculer la somme des éléments de y2 ;
	\item Remplacer le premier élément de y2 par un 4 ;
	\item Remplacer le premier élément de y2 par la valeur NA, puis calculer la somme des éléments de y2 ;
	\item Lister les objets en mémoire dans la session R ;
	\item Supprimer le vecteur les 4 vecteurs y1, y2, y3 et y4;
	\item Supprimer la totalité des objets de la session.
	\end{itemize}
\end{itemize}


\end{document}